#pragma once
#include<stdio.h>
#include<glew.h>
#include<glfw3.h>

class Window
{
public:
	Window();
	Window(GLint windowWidth, GLint windowHeight);
	int Initialise();
	GLfloat getBufferWidth() { return bufferWidth; }
	GLfloat getBufferHeight() { return bufferHeight; }
	GLfloat getXChange();
	GLfloat getYChange();

	//Movimiento
	GLfloat getx() { return x; }
	GLfloat gety() { return y; }
	GLfloat getz() { return z; }

	/*//Rotación
	GLfloat getrotateBraDer() { return rotateBraDer; }
	GLfloat getrotateManDer() { return rotateManDer; }
	GLfloat getrotatePieDer() { return rotatePieDer; }
	GLfloat getrotateBraIzq() { return rotateBraIzq; }
	GLfloat getrotateManIzq() { return rotateManIzq; }
	GLfloat getrotatePieIzq() { return rotatePieIzq; }*/

	//Teclas animación
	bool getAnimJuegos() { return animJuegos; }
	bool getAnimBaile() { return animBaile; }
	bool getAnimSnoopy() { return animSnoopy; }

	//Luces kiosko
	bool getLucesKiosko() { return lucesKiosko; }
	bool getAnimacionKiosko() { return animacionKiosko; }

	//Lintera
	GLfloat getluzLinterna() { return luzLinterna; }

	bool getShouldClose() {
		return  glfwWindowShouldClose(mainWindow);}
	bool* getsKeys() { return keys; }
	void swapBuffers() { return glfwSwapBuffers(mainWindow); }
	
	~Window();
private: 
	GLFWwindow *mainWindow;
	GLint width, height;
	bool keys[1024];
	GLint bufferWidth, bufferHeight;
	void createCallbacks();
	GLfloat x, y, z;
	//GLfloat rotateBraDer, rotateBraIzq, rotateManDer, rotateManIzq, rotatePieDer, rotatePieIzq;
	GLfloat lastX;
	GLfloat lastY;
	GLfloat xChange;
	GLfloat yChange;
	GLfloat luzLinterna;
	bool animJuegos, animBaile, animSnoopy;
	bool lucesKiosko, animacionKiosko;
	bool mouseFirstMoved;
	static void ManejaTeclado(GLFWwindow* window, int key, int code, int action, int mode);
	static void ManejaMouse(GLFWwindow* window, double xPos, double yPos);

};

