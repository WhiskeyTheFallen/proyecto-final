#include <stdio.h>
#include <string.h>
#include "Musica.h"

Musica::Musica()
{

}

int Musica::Initialise()
{
	//Music
	// start the sound engine with default parameters
	engine = irrklang::createIrrKlangDevice();

	if (!engine)
	{
		printf("Could not startup engine\n");
		return 0; // error starting up the engine
	}
	
	//****************************** M�sica de fondo ***************************************
	musicaFondo = engine->addSoundSourceFromFile("Music/Charlie Brown.mp3");
	musicaFondo->setDefaultVolume(0.2f);
	charlieBrown = engine->play2D(musicaFondo, true, false, true);

	musicaFiesta = engine->addSoundSourceFromFile("Music/Dance.mp3");
	musicaFiesta->setDefaultVolume(0.4f);
	fiesta = engine->play2D(musicaFiesta, true, true, true);

	musicaBaseball = engine->addSoundSourceFromFile("Music/Baseball.mp3");
	musicaBaseball->setDefaultVolume(0.4f);
	baseball = engine->play2D(musicaBaseball, false, true, true);

	//****************************** Ba�o 1 ***************************************
	source = engine->addSoundSourceFromFile("Music/Banos.mp3");
	source->setDefaultVolume(0.4f);
	bano1 = engine->play3D(source,
		irrklang::vec3df(0, 0, 0), true, false, true);

	// La distancia m�nima sonido m�ximo volumen
	if (bano1) {
		bano1->setMinDistance(1.5f);
		bano1->setPosition(irrklang::vec3df(-20.5f, 0.0, 0.0f));
	}

	//****************************** Ba�o 2 ***************************************
	//source = engine->addSoundSourceFromFile("Music/Banos.mp3");
	source->setDefaultVolume(0.4f);
	bano2 = engine->play3D(source,
		irrklang::vec3df(0, 0, 0), true, false, true);

	// La distancia m�nima sonido m�ximo volumen
	if (bano2) {
		bano2->setMinDistance(1.5f);
		bano2->setPosition(irrklang::vec3df(20.0f, 0.0, 00.0f));
	}

	//****************************** Fuente 1 ***************************************
	source = engine->addSoundSourceFromFile("Music/Fuente.mp3");
	source->setDefaultVolume(0.5f);
	fuente1 = engine->play3D(source,
		irrklang::vec3df(0, 0, 0), true, false, true);

	// La distancia m�nima sonido m�ximo volumen
	if (fuente1) {
		fuente1->setMinDistance(3.0f);
		fuente1->setPosition(irrklang::vec3df(0.0f, 0.0, -20.5f));
	}

	//****************************** Fuente 2 ***************************************
	//source = engine->addSoundSourceFromFile("Music/Fuente.mp3");
	source->setDefaultVolume(0.5f);
	fuente2 = engine->play3D(source,
		irrklang::vec3df(0, 0, 0), true, false, true);

	// La distancia m�nima sonido m�ximo volumen
	if (fuente2) {
		fuente2->setMinDistance(3.0f);
		fuente2->setPosition(irrklang::vec3df(0.0f, 0.0, 20.5f));
	}

	//****************************** Juegos ***************************************
	source = engine->addSoundSourceFromFile("Music/Juegos.mp3");
	source->setDefaultVolume(1.0f);
	juegos = engine->play3D(source,
		irrklang::vec3df(0, 0, 0), true, false, true);

	// La distancia m�nima sonido m�ximo volumen
	if (juegos) {
		juegos->setMinDistance(3.0f);
		juegos->setPosition(irrklang::vec3df(20.5f, 0.0, -20.5f));
	}

	//****************************** Tren ***************************************
	source = engine->addSoundSourceFromFile("Music/Tren.mp3");
	source->setDefaultVolume(0.4f);
	tren = engine->play3D(source,
		irrklang::vec3df(0, 0, 0), true, false, true);

	// La distancia m�nima sonido m�ximo volumen
	if (tren) {
		tren->setMinDistance(2.0f);
		tren->setPosition(irrklang::vec3df(-20.5f, 0.0, 20.5f));
	}
		
}


void Musica::PosCamara(glm::vec3 pos, glm::vec3 normal)
{
	// Debemos mandarle la posici�n de la camara y hacia donde ve
	engine->setListenerPosition(
		irrklang::vec3df(pos[0], pos[1], pos[2]), 
		irrklang::vec3df(-normal[0], -normal[1], -normal[2])
	);
}

void Musica::SetMusicaFondo()
{
	if (fiesta != NULL)
	{
		fiesta->stop(); // release music stream.
		fiesta->drop();
		charlieBrown->setIsPaused(false);
		fiesta = engine->play2D(musicaFiesta, true, true, true);
	}
}

void Musica::SetMusicaFiesta()
{
	if (charlieBrown != NULL)
	{
		charlieBrown->stop(); // release music stream.
		charlieBrown->drop();
		fiesta->setIsPaused(false);
		charlieBrown = engine->play2D(musicaFondo, true, true, true);
	}
}

void Musica::SetMusicaBaseball()
{
	baseball->setIsPaused(false);
}

void Musica::UnsetMusicaBaseball()
{
	baseball->stop(); // release music stream.
	baseball->drop();
	baseball = engine->play2D(musicaBaseball, false, true, true);
}



void Musica::Drop()
{
	engine->drop();
}

Musica::~Musica()
{
}

