
#define STB_IMAGE_IMPLEMENTATION

#include <stdio.h>
#include <string.h>
#include <cmath>
#include <vector>

#include <glew.h>
#include <glfw3.h>

#include <glm.hpp>
#include <gtc\matrix_transform.hpp>
#include <gtc\type_ptr.hpp>
//para probar el importer
//#include<assimp/Importer.hpp>

#include "Window.h"
#include "Mesh.h"
#include "Musica.h"
#include "Shader_light.h"
#include "Camera.h"
#include "Texture.h"
#include "Sphere.h"

//Iluminaci�n
#include "CommonValues.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "Material.h"

#include"Model.h"
#include "Skybox.h"
#include"SpotLight.h"

const float toRadians = 3.14159265f / 180.0f;

//Pelota
Sphere Pelota = Sphere(0.05, 20, 20);

//Variables para animaciones

//Keyframes
float reproduciranimacion, habilitaranimacion, guardoFrame, reinicioFrame, ciclo, ciclo2, contador = 0;

//Juegos y baile
float angColumpio;
float angRueda;
float angExt;
float angExt2;
float angPer;
float extremidadesOffset;
float juegosOffset;
float perOffset;
bool swing;
bool edo1;
bool edo2;
bool edo3;
bool edo4;

//Snoopy
float movxS;
float movyS;
float movzS;
float theta;
float modAltura;
float rotaSnoopy;
float angOffset;
float rotaOffset;

//Window
Window mainWindow;
std::vector<Mesh*> meshList;
std::vector<Shader> shaderList;

//Camara
Camera camera;

//Musica
Musica musica;

//Texturas
Texture plainTexture;
Texture pisoTexture;
Texture cafe;
Texture magenta;
Texture madera;
Texture oro;

//Materiales
Material Material_brillante;
Material Material_opaco;

//luz direccional
DirectionalLight mainLight;

//para declarar varias luces de tipo pointlight
PointLight pointLights[MAX_POINT_LIGHTS];
SpotLight spotLights[MAX_SPOT_LIGHTS];

//Lego Kiosko
Model candelabro;
Model barandal;
Model domo;

//Lego standar
Model Base_M;
Model Rueda_M;
Model PieDer_M;
Model PieIzq_M;
Model ManDer_M;
Model ManIzq_M;

//Modelo Charly
Model CharlyTorzo_M;
Model CharlyHead_M;
Model CharlyBraDer_M;
Model CharlyBraIzq_M;

//Modelo Player
Model PlayerTorzo_M;
Model PlayerHead_M;
Model PlayerBraDer_M;
Model PlayerBraIzq_M;
Model Bate_M;

//Modelo Linus
Model LinusTorzo_M;
Model LinusHead_M;
Model LinusBraDer_M;
Model LinusBraIzq_M;

//Snoopy
Model Snoopy_M;

//Elementos alameda
Model Rejas_M;
Model Fuentes_M;
Model Plantas_M;
Model Tren_M;
Model JB_M;
Model Postes_M;
Model Bath1_M;
Model Bath2_M;
Model Trash_M;
Model BaseColumpio_M;
Model Columpio_M;
Model JBaseRueda_M;
Model JRueda_M;
Model PosteL_M;
Model Foco_M;

//Lampara
Model lampara;

//Skybox
Skybox skybox = Skybox();

//Variables tiempo
GLfloat deltaTime = 0.0f;
GLfloat lastTime = 0.0f;

//void my_input(GLFWwindow *window);
void inputKeyframes(bool* keys);

// Vertex Shader
static const char* vShader = "shaders/shader_light.vert";

// Fragment Shader
static const char* fShader = "shaders/shader_light.frag";

//c�lculo del promedio de las normales para sombreado de Phong
void calcAverageNormals(unsigned int * indices, unsigned int indiceCount, GLfloat * vertices, unsigned int verticeCount,
	unsigned int vLength, unsigned int normalOffset)
{
	for (size_t i = 0; i < indiceCount; i += 3)
	{
		unsigned int in0 = indices[i] * vLength;
		unsigned int in1 = indices[i + 1] * vLength;
		unsigned int in2 = indices[i + 2] * vLength;
		glm::vec3 v1(vertices[in1] - vertices[in0], vertices[in1 + 1] - vertices[in0 + 1], vertices[in1 + 2] - vertices[in0 + 2]);
		glm::vec3 v2(vertices[in2] - vertices[in0], vertices[in2 + 1] - vertices[in0 + 1], vertices[in2 + 2] - vertices[in0 + 2]);
		glm::vec3 normal = glm::cross(v1, v2);
		normal = glm::normalize(normal);

		in0 += normalOffset; in1 += normalOffset; in2 += normalOffset;
		vertices[in0] += normal.x; vertices[in0 + 1] += normal.y; vertices[in0 + 2] += normal.z;
		vertices[in1] += normal.x; vertices[in1 + 1] += normal.y; vertices[in1 + 2] += normal.z;
		vertices[in2] += normal.x; vertices[in2 + 1] += normal.y; vertices[in2 + 2] += normal.z;
	}

	for (size_t i = 0; i < verticeCount / vLength; i++)
	{
		unsigned int nOffset = i * vLength + normalOffset;
		glm::vec3 vec(vertices[nOffset], vertices[nOffset + 1], vertices[nOffset + 2]);
		vec = glm::normalize(vec);
		vertices[nOffset] = vec.x; vertices[nOffset + 1] = vec.y; vertices[nOffset + 2] = vec.z;
	}
}

void CreateObjects()
{
	unsigned int indices[] = {
		0, 3, 1,
		1, 3, 2,
		2, 3, 0,
		0, 1, 2
	};

	GLfloat vertices[] = {
		//	x      y      z			u	  v			nx	  ny    nz
			-1.0f, -1.0f, -0.6f,	0.0f, 0.0f,		0.0f, 0.0f, 0.0f,
			0.0f, -1.0f, 1.0f,		0.5f, 0.0f,		0.0f, 0.0f, 0.0f,
			1.0f, -1.0f, -0.6f,		1.0f, 0.0f,		0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,		0.5f, 1.0f,		0.0f, 0.0f, 0.0f
	};

	unsigned int floorIndices[] = {
		0, 2, 1,
		1, 2, 3
	};

	GLfloat floorVertices[] = {
		-10.0f, 0.0f, -10.0f,	0.0f, 0.0f,		0.0f, -1.0f, 0.0f,
		10.0f, 0.0f, -10.0f,	1.0f, 0.0f,		0.0f, -1.0f, 0.0f,
		-10.0f, 0.0f, 10.0f,	0.0f, 1.0f,		0.0f, -1.0f, 0.0f,
		10.0f, 0.0f, 10.0f,		1.0f, 1.0f,		0.0f, -1.0f, 0.0f
	};

	calcAverageNormals(indices, 12, vertices, 32, 8, 5);

	Mesh *obj1 = new Mesh();
	obj1->CreateMesh(vertices, indices, 32, 12);
	meshList.push_back(obj1); //mesh 0

	Mesh *obj2 = new Mesh();
	obj2->CreateMesh(vertices, indices, 32, 12);
	meshList.push_back(obj2); //mesh 1

	Mesh *obj3 = new Mesh();
	obj3->CreateMesh(floorVertices, floorIndices, 32, 6);
	meshList.push_back(obj3);//mesh 2

}

void crearOctagono()
{
	unsigned int indices[] = {
		//Arriba-1
		0, 1, 2,
		//Arriba-2
		3, 4, 5,
		//Arriba-3
		6, 7, 8,
		//Arriba-4
		9, 10, 11,
		//Arriba-5
		12, 13, 14,
		//Arriba-6
		15, 16, 17,
		//Arriba-7
		18, 19, 20,
		//Arriba-8
		21, 22, 23,

		//Abajo-1
		24, 25, 26,
		//Abajo-2
		27, 28, 29,
		//Abajo-3
		30, 31, 32,
		//Abajo-4
		33, 34, 35,
		//Abajo-5
		36, 37, 38,
		//Abajo-6
		39, 40, 41,
		//Abajo-7
		42, 43, 44,
		//Abajo-8
		45, 46, 47,

		//Lado-1
		48, 49, 50,
		49, 51, 50,

		//Lado-2
		52, 53, 54,
		53, 55, 54,

		//Lado-3
		56, 57, 58,
		57, 59, 58,

		//Lado-4
		60, 61, 62,
		61, 63, 62,

		//Lado-5
		64, 65, 66,
		65, 67, 66,

		//Lado-6
		68, 69, 70,
		69, 71, 70,

		//Lado-7
		72, 73, 74,
		73, 75, 74,

		//Lado-8
		76, 77, 78,
		77, 79, 78,
	};

	GLfloat vertices[] = {
		//Arriba-1
		-3.28f, 1.0f, -7.96f,	0.196f, 0.0f,		0.0f, -1.0f, 0.0f,
		3.28f, 1.0f, -7.96f,	0.47f, 0.0f,		0.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,		0.33f, 0.5f,		0.0f, -1.0f, 0.0f,
		//Arriba-2
		3.28f, 1.0f, -7.96f,	0.47f, 0.0f,		0.0f, -1.0f, 0.0f,
		7.94f, 1.0f, -3.26f,	0.66f, 0.296f,		0.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,		0.33f, 0.5f,		0.0f, -1.0f, 0.0f,
		//Arriba-3
		7.94f, 1.0f, -3.26f,	0.66f, 0.296f,		0.0f, -1.0f, 0.0f,
		7.94f, 1.0f, 3.26f,		0.66f, 0.703f,		0.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,		0.33f, 0.5f,		0.0f, -1.0f, 0.0f,
		//Arriba-4
		7.94f, 1.0f, 3.26f,		0.66f, 0.703f,		0.0f, -1.0f, 0.0f,
		3.28f, 1.0f, 7.96f,		0.47f, 1.0f,		0.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,		0.33f, 0.5f,		0.0f, -1.0f, 0.0f,
		//Arriba-5
		3.28f, 1.0f, 7.96f,		0.47f, 1.0f,		0.0f, -1.0f, 0.0f,
		-3.28f, 1.0f,  7.96f,	0.196f, 1.0f,		0.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,		0.33f, 0.5f,		0.0f, -1.0f, 0.0f,
		//Arriba-6
		-3.28f, 1.0f, 7.96f,	0.196f, 1.0f,		0.0f, -1.0f, 0.0f,
		-7.94f, 1.0f, 3.26f,	0.0f, 0.703f,		0.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,		0.33f, 0.5f,		0.0f, -1.0f, 0.0f,
		//Arriba-7
		-7.94f, 1.0f, 3.26f,	0.0f, 0.703f,		0.0f, -1.0f, 0.0f,
		-7.94f, 1.0f, -3.26f,	0.0f, 0.296f,		0.0f, -1.0f, 0.0f,
		0.0f, 1.0f,  0.0f,		0.33f, 0.5f,		0.0f, -1.0f, 0.0f,
		//Arriba-8
		-7.94f, 1.0f, -3.26f,	0.0f, 0.296f,		0.0f, -1.0f, 0.0f,
		-3.28f, 1.0f, -7.96f,	0.196f, 0.0f,		0.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,		0.33f, 0.5f,		0.0f, -1.0f, 0.0f,

		//Abajo-1
		-3.28f, -1.0f, -7.96f,	0.196f, 0.0f,		0.0f, 1.0f, 0.0f,
		3.28f, -1.0f, -7.96f,	0.47f, 0.0f,		0.0f, 1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,		0.33f, 0.5f,		0.0f, 1.0f, 0.0f,
		//Abajo-2
		3.28f, -1.0f, -7.96f,	0.47f, 0.0f,		0.0f, 1.0f, 0.0f,
		7.94f, -1.0f, -3.26f,	0.66f, 0.296f,		0.0f, 1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,		0.33f, 0.5f,		0.0f, 1.0f, 0.0f,
		//Abajo-3
		7.94f, -1.0f, -3.26f,	0.66f, 0.296f,		0.0f, 1.0f, 0.0f,
		7.94f, -1.0f, 3.26f,	0.66f, 0.703f,		0.0f, 1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,		0.33f, 0.5f,		0.0f, 1.0f, 0.0f,
		//Abajo-4
		7.94f, -1.0f, 3.26f,	0.66f, 0.703f,		0.0f, 1.0f, 0.0f,
		3.28f, -1.0f, 7.96f,	0.47f, 1.0f,		0.0f, 1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,		0.33f, 0.5f,		0.0f, 1.0f, 0.0f,
		//Abajo-5
		3.28f, -1.0f, 7.96f,	0.47f, 1.0f,		0.0f, 1.0f, 0.0f,
		-3.28f, -1.0f,  7.96f,	0.196f, 1.0f,		0.0f, 1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,		0.33f, 0.5f,		0.0f, 1.0f, 0.0f,
		//Abajo-6
		-3.28f, -1.0f, 7.96f,	0.196f, 1.0f,		0.0f, 1.0f, 0.0f,
		-7.94f, -1.0f, 3.26f,	0.0f, 0.703f,		0.0f, 1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,		0.33f, 0.5f,		0.0f, 1.0f, 0.0f,
		//Abajo-7
		-7.94f, -1.0f, 3.26f,	0.0f, 0.703f,		0.0f, 1.0f, 0.0f,
		-7.94f, -1.0f, -3.26f,	0.0f, 0.296f,		0.0f, 1.0f, 0.0f,
		0.0f, -1.0f,  0.0f,		0.33f, 0.5f,		0.0f, 1.0f, 0.0f,
		//Abajo-8
		-7.94f, -1.0f, -3.26f,	0.0f, 0.296f,		0.0f, 1.0f, 0.0f,
		-3.28f, -1.0f, -7.96f,	0.196f, 0.0f,		0.0f, 1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,		0.33f, 0.5f,		0.0f, 1.0f, 0.0f,

		//Lado-1
		-3.28f, -1.0f, -7.96f,	1.0f, 0.0f,		0.0f, 0.0f, 1.0f,
		3.28f, -1.0f, -7.96f,	1.0f, 1.0f,		0.0f, 0.0f, 1.0f,
		-3.28f, 1.0f, -7.96f,	1.0f, 0.0f,		0.0f, 0.0f, 1.0f,
		3.28f, 1.0f, -7.96f,	1.0f, 1.0f,		0.0f, 0.0f, 1.0f,

		//Lado-2
		3.28f, -1.0f, -7.96f,	1.0f, 0.0f,		-1.0f, 0.0f, 1.0f,
		7.94f, -1.0f, -3.26f,	1.0f, 1.0f,		-1.0f, 0.0f, 1.0f,
		3.28f, 1.0f, -7.96f,	1.0f, 0.0f,		-1.0f, 0.0f, 1.0f,
		7.94f, 1.0f, -3.26f,	1.0f, 1.0f,		-1.0f, 0.0f, 1.0f,

		//Lado-3
		7.94f, -1.0f, -3.26f,	1.0f, 1.0f,		-1.0f, 0.0f, 0.0f,
		7.94f, -1.0f, 3.26f,	1.0f, 1.0f,		-1.0f, 0.0f, 0.0f,
		7.94f, 1.0f, -3.26f,	1.0f, 1.0f,		-1.0f, 0.0f, 0.0f,
		7.94f, 1.0f, 3.26f,		1.0f, 1.0f,		-1.0f, 0.0f, 0.0f,

		//Lado-4
		7.94f, -1.0f, 3.26f,	1.0f, 1.0f,		-1.0f, 0.0f, -1.0f,
		3.28f, -1.0f, 7.96f,	1.0f, 1.0f,		-1.0f, 0.0f, -1.0f,
		7.94f, 1.0f, 3.26f,		1.0f, 1.0f,		-1.0f, 0.0f, -1.0f,
		3.28f, 1.0f, 7.96f,		1.0f, 1.0f,		-1.0f, 0.0f, -1.0f,

		//Lado-5
		3.28f, -1.0f, 7.96f,	1.0f, 1.0f,		0.0f, 0.0f, -1.0f,
		-3.28f, -1.0f,  7.96f,	1.0f, 1.0f,		0.0f, 0.0f, -1.0f,
		3.28f, 1.0f, 7.96f,		1.0f, 1.0f,		0.0f, 0.0f, -1.0f,
		-3.28f, 1.0f,  7.96f,	1.0f, 1.0f,		0.0f, 0.0f, -1.0f,

		//Lado-6
		-3.28f, -1.0f, 7.96f,	1.0f, 1.0f,		1.0f, 0.0f, -1.0f,
		-7.94f, -1.0f, 3.26f,	1.0f, 1.0f,		1.0f, 0.0f, -1.0f,
		-3.28f, 1.0f, 7.96f,	1.0f, 1.0f,		1.0f, 0.0f, -1.0f,
		-7.94f, 1.0f, 3.26f,	1.0f, 1.0f,		1.0f, 0.0f, -1.0f,

		//Lado-7
		-7.94f, -1.0f, 3.26f,	1.0f, 1.0f,		1.0f, 0.0f, 0.0f,
		-7.94f, -1.0f, -3.26f,	1.0f, 1.0f,		1.0f, 0.0f, 0.0f,
		-7.94f, 1.0f, 3.26f,	1.0f, 1.0f,		1.0f, 0.0f, 0.0f,
		-7.94f, 1.0f, -3.26f,	1.0f, 1.0f,		1.0f, 0.0f, 0.0f,

		//Lado-8
		-7.94f, -1.0f, -3.26f,	1.0f, 1.0f,		1.0f, 0.0f, 1.0f,
		-3.28f, -1.0f, -7.96f,	1.0f, 0.0f,		1.0f, 0.0f, 1.0f,
		-7.94f, 1.0f, -3.26f,	1.0f, 1.0f,		1.0f, 0.0f, 1.0f,
		-3.28f, 1.0f, -7.96f,	1.0f, 0.0f,		1.0f, 0.0f, 1.0f,
	};

	Mesh *octagono = new Mesh();
	octagono->CreateMesh(vertices, indices, 640, 102);
	meshList.push_back(octagono); //mesh 3
}

void crearPrimaTriangular()
{
	unsigned int indices[] = {
		//Arriba-1
		0, 1, 2,
		//Arriba-2
		3, 4, 5,
		//Arriba-3
		6, 7, 8,
		//Arriba-4
		9, 10, 11,
		//Arriba-5
		12, 13, 14,
		//Arriba-6
		15, 16, 17,
		//Arriba-7
		18, 19, 20,
		//Arriba-8
		21, 22, 23,
	};

	GLfloat vertices[] = {
		//Arriba-1
		-3.28f, 1.0f, -7.96f,	0.196f, 0.0f,		0.0f, -1.0f, 1.0f,
		3.28f, 1.0f, -7.96f,	0.47f, 0.0f,		0.0f, -1.0f, 1.0f,
		0.0f, 9.0f, 0.0f,		0.33f, 0.5f,		0.0f, -1.0f, 1.0f,
		//Arriba-2
		3.28f, 1.0f, -7.96f,	0.47f, 0.0f,		-1.0f, -1.0f, 1.0f,
		7.94f, 1.0f, -3.26f,	0.66f, 0.296f,		-1.0f, -1.0f, 1.0f,
		0.0f, 9.0f, 0.0f,		0.33f, 0.5f,		-1.0f, -1.0f, 1.0f,
		//Arriba-3
		7.94f, 1.0f, -3.26f,	0.66f, 0.296f,		-1.0f, -1.0f, 0.0f,
		7.94f, 1.0f, 3.26f,		0.66f, 0.703f,		-1.0f, -1.0f, 0.0f,
		0.0f, 9.0f, 0.0f,		0.33f, 0.5f,		-1.0f, -1.0f, 0.0f,
		//Arriba-4
		7.94f, 1.0f, 3.26f,		0.66f, 0.703f,		-1.0f, -1.0f, -1.0f,
		3.28f, 1.0f, 7.96f,		0.47f, 1.0f,		-1.0f, -1.0f, -1.0f,
		0.0f, 9.0f, 0.0f,		0.33f, 0.5f,		-1.0f, -1.0f, -1.0f,
		//Arriba-5
		3.28f, 1.0f, 7.96f,		0.47f, 1.0f,		0.0f, -1.0f, -1.0f,
		-3.28f, 1.0f,  7.96f,	0.196f, 1.0f,		0.0f, -1.0f, -1.0f,
		0.0f, 9.0f, 0.0f,		0.33f, 0.5f,		0.0f, -1.0f, -1.0f,
		//Arriba-6
		-3.28f, 1.0f, 7.96f,	0.196f, 1.0f,		1.0f, -1.0f, -1.0f,
		-7.94f, 1.0f, 3.26f,	0.0f, 0.703f,		1.0f, -1.0f, -1.0f,
		0.0f, 9.0f, 0.0f,		0.33f, 0.5f,		1.0f, -1.0f, -1.0f,
		//Arriba-7
		-7.94f, 1.0f, 3.26f,	0.0f, 0.703f,		1.0f, -1.0f, 0.0f,
		-7.94f, 1.0f, -3.26f,	0.0f, 0.296f,		1.0f, -1.0f, 0.0f,
		0.0f, 9.0f,  0.0f,		0.33f, 0.5f,		1.0f, -1.0f, 0.0f,
		//Arriba-8
		-7.94f, 1.0f, -3.26f,	0.0f, 0.296f,		1.0f, -1.0f, 1.0f,
		-3.28f, 1.0f, -7.96f,	0.196f, 0.0f,		1.0f, -1.0f, 1.0f,
		0.0f, 9.0f, 0.0f,		0.33f, 0.5f,		1.0f, -1.0f, 1.0f,
	};

	Mesh *prisma = new Mesh();
	prisma->CreateMesh(vertices, indices, 192, 24);
	meshList.push_back(prisma); //mesh 4

}

void crearCubo()
{
	unsigned int cubo_indices[] = {
		// front
		0, 1, 2,
		2, 3, 0,
		// right
		4, 5, 6,
		6, 7, 4,
		// back
		8, 9, 10,
		10, 11, 8,

		// left
		12, 13, 14,
		14, 15, 12,
		// bottom
		16, 17, 18,
		18, 19, 16,
		// top
		20, 21, 22,
		22, 23, 20,
	};

	GLfloat cubo_vertices[] = {
		// front
		//x		y		z		S		T			NX		NY		NZ
		-0.5f, -0.5f,  0.5f,	0.0f,	0.0f,		0.0f,	0.0f,	-1.0f,
		0.5f, -0.5f,  0.5f,		1.0f,	0.0f,		0.0f,	0.0f,	-1.0f,
		0.5f,  0.5f,  0.5f,		1.0f,	1.0f,		0.0f,	0.0f,	-1.0f,
		-0.5f,  0.5f,  0.5f,	0.0f,	1.0f,		0.0f,	0.0f,	-1.0f,
		// right
		//x		y		z		S		T
		0.5f, -0.5f,  0.5f,	    0.0f,	0.0f,		-1.0f,	0.0f,	0.0f,
		0.5f, -0.5f,  -0.5f,	1.0f,	0.0f,		-1.0f,	0.0f,	0.0f,
		0.5f,  0.5f,  -0.5f,	1.0f,	1.0f,		-1.0f,	0.0f,	0.0f,
		0.5f,  0.5f,  0.5f,		0.0f,	1.0f,		-1.0f,	0.0f,	0.0f,
		// back
		-0.5f, -0.5f, -0.5f,	0.0f,	0.0f,		0.0f,	0.0f,	1.0f,
		0.5f, -0.5f, -0.5f,		1.0f,	0.0f,		0.0f,	0.0f,	1.0f,
		0.5f,  0.5f, -0.5f,		1.0f,	1.0f,		0.0f,	0.0f,	1.0f,
		-0.5f,  0.5f, -0.5f,	0.0f,	1.0f,		0.0f,	0.0f,	1.0f,

		// left
		//x		y		z		S		T
		-0.5f, -0.5f,  -0.5f,	0.0f,	0.0f,		1.0f,	0.0f,	0.0f,
		-0.5f, -0.5f,  0.5f,	1.0f,	0.0f,		1.0f,	0.0f,	0.0f,
		-0.5f,  0.5f,  0.5f,	1.0f,	1.0f,		1.0f,	0.0f,	0.0f,
		-0.5f,  0.5f,  -0.5f,	0.0f,	1.0f,		1.0f,	0.0f,	0.0f,

		// bottom
		//x		y		z		S		T
		-0.5f, -0.5f,  0.5f,	0.0f,	0.0f,		0.0f,	1.0f,	0.0f,
		0.5f,  -0.5f,  0.5f,	1.0f,	0.0f,		0.0f,	1.0f,	0.0f,
		 0.5f,  -0.5f,  -0.5f,	1.0f,	1.0f,		0.0f,	1.0f,	0.0f,
		-0.5f, -0.5f,  -0.5f,	0.0f,	1.0f,		0.0f,	1.0f,	0.0f,

		//UP
		 //x	y		z		S		T
		 -0.5f, 0.5f,  0.5f,	0.0f,	0.0f,		0.0f,	-1.0f,	0.0f,
		 0.5f,  0.5f,  0.5f,	1.0f,	0.0f,		0.0f,	-1.0f,	0.0f,
		  0.5f, 0.5f,  -0.5f,	1.0f,	1.0f,		0.0f,	-1.0f,	0.0f,
		 -0.5f, 0.5f,  -0.5f,	0.0f,	1.0f,		0.0f,	-1.0f,	0.0f,

	};
	//calcAverageNormals(cubo_indices, 36, cubo_vertices, 192, 8, 5);
	Mesh *cubo = new Mesh();
	cubo->CreateMesh(cubo_vertices, cubo_indices, 192, 36);
	meshList.push_back(cubo); //mesh 3

}

void CreateShaders()
{
	Shader *shader1 = new Shader();
	shader1->CreateFromFiles(vShader, fShader);
	shaderList.push_back(*shader1);
}

///////////////////////////////KEYFRAMES/////////////////////


bool animacion = false;

float angulo = 15.0f;
//float angulo1 = 15.0f;
//float angulo2 = 15.0f;

//NEW// Keyframes

float giroPlayer = 180.0f, giroPier1 = 0.0f, giroPier2 = 0.0f, xPelota = 0.18f, yPelota = -1.5f, zPelota = 1.0f, giroBra = 0.0f;
float giroBra1 = 0.0f, giroCab = 0.0f, giroCH = 0.0f, giroPlayery = 0.0f, giroCHy = 0.0f;

#define MAX_FRAMES 40
int i_max_steps = 20; //Cambiar aqui para velocidad animacion
int i_curr_steps = 21;
typedef struct _frame
{
	//Variables para GUARDAR Key Frames
	float xPelota;		//Variable para PosicionX
	float yPelota;		//Variable para PosicionY
	float zPelota;		//Variable para PosicionZ
	float mov_xInc;		//Variable para IncrementoX
	float mov_yInc;		//Variable para IncrementoY
	float mov_zInc;		//Variable para IncrementoZ
	float giroPlayer;
	float giroPlayerInc;
	float giroPlayery;
	float giroPlayeryInc;
	float giroPier1;
	float giroPier1Inc;
	float giroPier2;
	float giroPier2Inc;
	float giroBra;
	float giroBraInc;
	float giroBra1;
	float giroBra1Inc;
	float giroCab;
	float giroCabInc;
	float giroCH;
	float giroCHInc;
	float giroCHy;
	float giroCHyInc;
}FRAME;

FRAME KeyFrame[MAX_FRAMES];
int FrameIndex = 39;			//introducir datos
bool play = false;
int playIndex = 0;

void saveFrame(void)
{

	printf("frameindex %d\n", FrameIndex);


	KeyFrame[FrameIndex].xPelota = xPelota;
	KeyFrame[FrameIndex].yPelota = yPelota;
	KeyFrame[FrameIndex].zPelota = zPelota;
	KeyFrame[FrameIndex].giroPlayer = giroPlayer;
	KeyFrame[FrameIndex].giroPlayery = giroPlayery;
	KeyFrame[FrameIndex].giroPier1 = giroPier1;
	KeyFrame[FrameIndex].giroPier2 = giroPier2;
	KeyFrame[FrameIndex].giroBra = giroBra;
	KeyFrame[FrameIndex].giroCab = giroCab;
	KeyFrame[FrameIndex].giroCH = giroCH;
	KeyFrame[FrameIndex].giroCHy = giroCHy;

	FrameIndex++;
}

void resetElements(void)
{

	xPelota = KeyFrame[0].xPelota;
	yPelota = KeyFrame[0].yPelota;
	zPelota = KeyFrame[0].zPelota;
	giroPlayer = KeyFrame[0].giroPlayer;
	giroPlayery = KeyFrame[0].giroPlayery;
	giroPier1 = KeyFrame[0].giroPier1;
	giroPier2 = KeyFrame[0].giroPier2;
	giroBra = KeyFrame[0].giroBra;
	giroBra1 = KeyFrame[0].giroBra1;
	giroCab = KeyFrame[0].giroCab;
	giroCH = KeyFrame[0].giroCH;
	giroCHy = KeyFrame[0].giroCHy;
}

void interpolation(void)
{
	KeyFrame[playIndex].mov_xInc = (KeyFrame[playIndex + 1].xPelota - KeyFrame[playIndex].xPelota) / i_max_steps;
	KeyFrame[playIndex].mov_yInc = (KeyFrame[playIndex + 1].yPelota - KeyFrame[playIndex].yPelota) / i_max_steps;
	KeyFrame[playIndex].mov_zInc = (KeyFrame[playIndex + 1].zPelota - KeyFrame[playIndex].zPelota) / i_max_steps;
	KeyFrame[playIndex].giroPlayerInc = (KeyFrame[playIndex + 1].giroPlayer - KeyFrame[playIndex].giroPlayer) / i_max_steps;
	KeyFrame[playIndex].giroPlayeryInc = (KeyFrame[playIndex + 1].giroPlayery - KeyFrame[playIndex].giroPlayery) / i_max_steps;
	KeyFrame[playIndex].giroPier1Inc = (KeyFrame[playIndex + 1].giroPier1 - KeyFrame[playIndex].giroPier1) / i_max_steps;
	KeyFrame[playIndex].giroPier2Inc = (KeyFrame[playIndex + 1].giroPier2 - KeyFrame[playIndex].giroPier2) / i_max_steps;
	KeyFrame[playIndex].giroBraInc = (KeyFrame[playIndex + 1].giroBra - KeyFrame[playIndex].giroBra) / i_max_steps;
	KeyFrame[playIndex].giroBra1Inc = (KeyFrame[playIndex + 1].giroBra1 - KeyFrame[playIndex].giroBra1) / i_max_steps;
	KeyFrame[playIndex].giroCabInc = (KeyFrame[playIndex + 1].giroCab - KeyFrame[playIndex].giroCab) / i_max_steps;
	KeyFrame[playIndex].giroCHInc = (KeyFrame[playIndex + 1].giroCH - KeyFrame[playIndex].giroCH) / i_max_steps;
	KeyFrame[playIndex].giroCHyInc = (KeyFrame[playIndex + 1].giroCHy - KeyFrame[playIndex].giroCHy) / i_max_steps;
}


void animate(void)
{
	//Movimiento del objeto
	if (play)
	{
		if (i_curr_steps >= i_max_steps) //end of animation between frames?
		{
			playIndex++;
			printf("playindex : %d\n", playIndex);
			if (playIndex > FrameIndex - 2)	//end of total animation?
			{
				printf("Frame index= %d\n", FrameIndex);
				printf("termina anim\n");
				musica.UnsetMusicaBaseball();
				playIndex = 0;
				play = false;
			}
			else //Next frame interpolations
			{
				//printf("entro aqu�\n");
				i_curr_steps = 0; //Reset counter
				//Interpolation
				interpolation();
			}
		}
		else
		{
			//printf("se qued� aqui\n");
			//printf("max steps: %f", i_max_steps);
			//Draw animation
			xPelota += KeyFrame[playIndex].mov_xInc;
			yPelota += KeyFrame[playIndex].mov_yInc;
			zPelota += KeyFrame[playIndex].mov_zInc;
			giroPlayer += KeyFrame[playIndex].giroPlayerInc;
			giroPlayery += KeyFrame[playIndex].giroPlayeryInc;
			giroPier1 += KeyFrame[playIndex].giroPier1Inc;
			giroPier2 += KeyFrame[playIndex].giroPier2Inc;
			giroBra += KeyFrame[playIndex].giroBraInc;
			giroBra1 += KeyFrame[playIndex].giroBra1Inc;
			giroCab += KeyFrame[playIndex].giroCabInc;
			giroCH += KeyFrame[playIndex].giroCHInc;
			giroCHy += KeyFrame[playIndex].giroCHyInc;
			i_curr_steps++;
		}

	}
}

/* FIN KEYFRAMES*/

int main()
{
	//Inicializar window
	mainWindow = Window(1366, 768); // 1280, 1024 or 1024, 768
	mainWindow.Initialise();

	CreateObjects();
	crearOctagono();
	crearCubo();
	crearPrimaTriangular();
	Pelota.init();
	Pelota.load();
	CreateShaders();

	//Color
	GLuint uniformColor = 0;

	//Inicializar camera
	camera = Camera(glm::vec3(0.0f, 1.0f, 2.0f), glm::vec3(0.0f, 1.0f, 0.0f), -60.0f, 0.0f, 5.0f, 0.5f);

	//Inicializar musica
	musica = Musica();
	musica.Initialise();

	//Material
	Material_brillante = Material(4.0f, 256);
	Material_opaco = Material(0.3f, 4);

	//************************ Texturas *****************************************

	//Piso alameda
	plainTexture = Texture("Textures/alameda.png");
	plainTexture.LoadTextureA();

	//Kiosko
	cafe = Texture("Textures/cafe.tga");
	cafe.LoadTextureA();
	magenta = Texture("Textures/magenta.tga");
	magenta.LoadTextureA();
	madera = Texture("Textures/madera.tga");
	madera.LoadTextureA();
	oro = Texture("Textures/oro.tga");
	oro.LoadTextureA();

	//********************************* Modelos **********************************

	//Lego personajes
	Base_M = Model();
	Base_M.LoadModel("Models/Charly_Brown/Base.obj");
	Rueda_M = Model();
	Rueda_M.LoadModel("Models/Charly_Brown/Rueda.obj");
	PieDer_M = Model();
	PieDer_M.LoadModel("Models/Charly_Brown/PieDer.obj");
	PieIzq_M = Model();
	PieIzq_M.LoadModel("Models/Charly_Brown/PieIzq.obj");
	ManDer_M = Model();
	ManDer_M.LoadModel("Models/Charly_Brown/ManDer.obj");
	ManIzq_M = Model();
	ManIzq_M.LoadModel("Models/Charly_Brown/ManIzq.obj");

	//Charly
	CharlyTorzo_M = Model();
	CharlyTorzo_M.LoadModel("Models/Charly_Brown/Charly_torzo.obj");
	CharlyHead_M = Model();
	CharlyHead_M.LoadModel("Models/Charly_Brown/Charly_head.obj");
	CharlyBraDer_M = Model();
	CharlyBraDer_M.LoadModel("Models/Charly_Brown/Charly_braDer.obj");
	CharlyBraIzq_M = Model();
	CharlyBraIzq_M.LoadModel("Models/Charly_Brown/Charly_braIzq.obj");

	//Linus
	LinusTorzo_M = Model();
	LinusTorzo_M.LoadModel("Models/Linus/Linus_torzo.obj");
	LinusHead_M = Model();
	LinusHead_M.LoadModel("Models/Linus/Linus_head.obj");
	LinusBraDer_M = Model();
	LinusBraDer_M.LoadModel("Models/Linus/Linus_braDer.obj");
	LinusBraIzq_M = Model();
	LinusBraIzq_M.LoadModel("Models/Linus/Linus_braIzq.obj");

	//Player
	PlayerTorzo_M = Model();
	PlayerTorzo_M.LoadModel("Models/Charly_Brown/Player_torzo.obj");
	PlayerHead_M = Model();
	PlayerHead_M.LoadModel("Models/Charly_Brown/Player_head.obj");
	PlayerBraDer_M = Model();
	PlayerBraDer_M.LoadModel("Models/Charly_Brown/Player_braDer.obj");
	PlayerBraIzq_M = Model();
	PlayerBraIzq_M.LoadModel("Models/Charly_Brown/Player_braIzq.obj");
	Bate_M = Model();
	Bate_M.LoadModel("Models/bate.obj");

	
	//Snoopy
	Snoopy_M = Model();
	Snoopy_M.LoadModel("Models/Snoopy/snoopy_vol.obj");

	//Kiosko
	candelabro = Model();
	candelabro.LoadModel("Models/Kiosko/candelabro.obj");
	barandal = Model();
	barandal.LoadModel("Models/Kiosko/barandal.obj");
	domo = Model();
	domo.LoadModel("Models/Kiosko/domo.obj");
	lampara = Model();
	lampara.LoadModel("Models/Kiosko/lampara.obj");

	//Basura
	Trash_M = Model();
	Trash_M.LoadModel("Models/Elementos_alameda/trash.obj");

	//Postes en L
	PosteL_M = Model();
	PosteL_M.LoadModel("Models/Elementos_alameda/lamp_L.obj");

	//Postes
	Postes_M = Model();
	Postes_M.LoadModel("Models/Elementos_alameda/luces.obj");

	//Ba�o
	Bath1_M = Model();
	Bath1_M.LoadModel("Models/Elementos_alameda/bathroom1.obj");
	Bath2_M = Model();
	Bath2_M.LoadModel("Models/Elementos_alameda/bathroom2.obj");

	//Foco ba�os
	Foco_M = Model();
	Foco_M.LoadModel("Models/Elementos_alameda/foco.obj");
	//Fuentes
	Fuentes_M = Model();
	Fuentes_M.LoadModel("Models/Elementos_alameda/fuentes.obj");

	//Rejas
	Rejas_M = Model();
	Rejas_M.LoadModel("Models/Elementos_alameda/rejas.obj");

	//Plantas
	Plantas_M = Model();
	Plantas_M.LoadModel("Models/Elementos_alameda/plantas.obj");

	//Tren
	Tren_M = Model();
	Tren_M.LoadModel("Models/Elementos_alameda/tren.obj");

	//Juegos y bancas
	JB_M = Model();
	JB_M.LoadModel("Models/Elementos_alameda/bancas_juegos.obj");

	//Columpio
	BaseColumpio_M = Model();
	BaseColumpio_M.LoadModel("Models/Elementos_alameda/base_columpio.obj");
	Columpio_M = Model();
	Columpio_M.LoadModel("Models/Elementos_alameda/columpio.obj");

	//Rueda juego
	JBaseRueda_M = Model();
	JBaseRueda_M.LoadModel("Models/Elementos_alameda/base_rueda.obj");
	JRueda_M = Model();
	JRueda_M.LoadModel("Models/Elementos_alameda/rueda.obj");

	//****************************** Luces ***************************************

	//luz direccional para el amanecer
	mainLight = DirectionalLight(1.0f, 1.0f, 1.0f,
		0.3f, 0.3f,
		0.0f, 0.0f, 1.0f);

	//contador de luces puntuales
	unsigned int pointLightCount = 0;

	//contador de luces spot
	unsigned int spotLightCount = 0;

	//*************************** Skybox ***************************************
	std::vector<std::string> Daybreak;
	Daybreak.push_back("Textures/Skybox/Daybreak/Right.tga");
	Daybreak.push_back("Textures/Skybox/Daybreak/Left.tga");
	Daybreak.push_back("Textures/Skybox/Daybreak/Down.tga");
	Daybreak.push_back("Textures/Skybox/Daybreak/Up.tga");
	Daybreak.push_back("Textures/Skybox/Daybreak/Back.tga");
	Daybreak.push_back("Textures/Skybox/Daybreak/Front.tga");

	std::vector<std::string> Evening;
	Evening.push_back("Textures/Skybox/Evening/Right.tga");
	Evening.push_back("Textures/Skybox/Evening/Left.tga");
	Evening.push_back("Textures/Skybox/Evening/Down.tga");
	Evening.push_back("Textures/Skybox/Evening/Up.tga");
	Evening.push_back("Textures/Skybox/Evening/Back.tga");
	Evening.push_back("Textures/Skybox/Evening/Front.tga");

	std::vector<std::string> Midday;
	Midday.push_back("Textures/Skybox/Midday/Right.tga");
	Midday.push_back("Textures/Skybox/Midday/Left.tga");
	Midday.push_back("Textures/Skybox/Midday/Down.tga");
	Midday.push_back("Textures/Skybox/Midday/Up.tga");
	Midday.push_back("Textures/Skybox/Midday/Back.tga");
	Midday.push_back("Textures/Skybox/Midday/Front.tga");

	std::vector<std::string> Midnight;
	Midnight.push_back("Textures/Skybox/Midnight/Right.tga");
	Midnight.push_back("Textures/Skybox/Midnight/Left.tga");
	Midnight.push_back("Textures/Skybox/Midnight/Down.tga");
	Midnight.push_back("Textures/Skybox/Midnight/Up.tga");
	Midnight.push_back("Textures/Skybox/Midnight/Back.tga");
	Midnight.push_back("Textures/Skybox/Midnight/Front.tga");

	std::vector<std::string> Sunset;
	Sunset.push_back("Textures/Skybox/Sunset/Right.tga");
	Sunset.push_back("Textures/Skybox/Sunset/Left.tga");
	Sunset.push_back("Textures/Skybox/Sunset/Down.tga");
	Sunset.push_back("Textures/Skybox/Sunset/Up.tga");
	Sunset.push_back("Textures/Skybox/Sunset/Back.tga");
	Sunset.push_back("Textures/Skybox/Sunset/Front.tga");

	skybox.createSkybox(Daybreak);

	GLuint uniformProjection = 0, uniformModel = 0, uniformView = 0, uniformEyePosition = 0,
		uniformSpecularIntensity = 0, uniformShininess = 0;
	glm::mat4 projection = glm::perspective(45.0f, (GLfloat)mainWindow.getBufferWidth() / mainWindow.getBufferHeight(), 0.1f, 300.0f);

	//Variables Luces Kiosko
	float luces = 0.0f;
	float movimientoVertical = 0;
	float movimientoHorizontal = 0;
	int indiceLuces = 0;
	int lapsoLuces = 0;

	//Variables Skybox
	bool accion = true;
	bool luminarias = true;
	bool color = true;
	int lapso = 15;
	int faseDia = 0;
	GLfloat adelante = 0;

	//Inicializaci�n variables de aniamci�n
	angColumpio = 0.0f;
	angRueda = 0.0f;
	angExt = 0.0f;
	angPer = 0.0f;
	angExt2 = 0.0f;
	extremidadesOffset = 3.0f;
	juegosOffset = 2.0f;
	perOffset = 3.0f;
	swing = true;
	edo1 = true;
	edo2 = false;
	edo3 = false;
	edo4 = false;

	//Snoopy variables
	movxS = 10.0f;
	movyS = 4.0f;
	movzS = 0.0f;
	theta = 0.0f;
	modAltura = 5.0f;
	rotaSnoopy = 0.0f;
	angOffset = 1.0f;
	rotaOffset = 1.0f;

	int num_key = 0;
	//KEYFRAMES DECLARADOS INICIALES

	KeyFrame[num_key].xPelota = 0.18f;
	KeyFrame[num_key].yPelota = -1.5f;
	KeyFrame[num_key].zPelota = 1.0f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.18f;
	KeyFrame[num_key].yPelota = 0.97f;
	KeyFrame[num_key].zPelota = 1.0f;
	KeyFrame[num_key].giroPlayer = 90.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 12.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.17f;
	KeyFrame[num_key].yPelota = 0.93f;
	KeyFrame[num_key].zPelota = 1.14f;
	KeyFrame[num_key].giroPlayer = 90.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 9.333f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.17f;
	KeyFrame[num_key].yPelota = 0.93f;
	KeyFrame[num_key].zPelota = 1.64f;
	KeyFrame[num_key].giroPlayer = 90.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 6.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.2f;
	KeyFrame[num_key].yPelota = 0.85f;
	KeyFrame[num_key].zPelota = 2.5f;
	KeyFrame[num_key].giroPlayer = 90.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 3.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.24f;
	KeyFrame[num_key].yPelota = 0.80f;
	KeyFrame[num_key].zPelota = 3.5f;
	KeyFrame[num_key].giroPlayer = 90.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 3.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.24f;
	KeyFrame[num_key].yPelota = 0.76f;
	KeyFrame[num_key].zPelota = 4.79f;
	KeyFrame[num_key].giroPlayer = 165.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 3.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -1.0f;
	KeyFrame[num_key].yPelota = 1.5f;
	KeyFrame[num_key].zPelota = 3.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -2.0f;
	KeyFrame[num_key].yPelota = 2.0f;
	KeyFrame[num_key].zPelota = 2.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -3.0f;
	KeyFrame[num_key].yPelota = 2.5f;
	KeyFrame[num_key].zPelota = 1.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -4.0f;
	KeyFrame[num_key].yPelota = 2.0f;
	KeyFrame[num_key].zPelota = 0.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -5.0f;
	KeyFrame[num_key].yPelota = 1.5f;
	KeyFrame[num_key].zPelota = -0.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -6.0f;
	KeyFrame[num_key].yPelota = 1.0f;
	KeyFrame[num_key].zPelota = -1.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -7.0f;
	KeyFrame[num_key].yPelota = 0.5f;
	KeyFrame[num_key].zPelota = -2.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -8.0f;
	KeyFrame[num_key].yPelota = 0.05f;
	KeyFrame[num_key].zPelota = -3.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;
	//Termina 1 lanzamiento
	KeyFrame[num_key].xPelota = 0.18f;
	KeyFrame[num_key].yPelota = -1.5f;
	KeyFrame[num_key].zPelota = 1.0f;
	KeyFrame[num_key].giroPlayer = 165.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 3.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.18f;
	KeyFrame[num_key].yPelota = 0.97f;
	KeyFrame[num_key].zPelota = 1.0f;
	KeyFrame[num_key].giroPlayer = 90.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 3.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 12.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.17f;
	KeyFrame[num_key].yPelota = 0.93f;
	KeyFrame[num_key].zPelota = 1.14f;
	KeyFrame[num_key].giroPlayer = 90.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 9.333f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.17f;
	KeyFrame[num_key].yPelota = 0.93f;
	KeyFrame[num_key].zPelota = 1.64f;
	KeyFrame[num_key].giroPlayer = 90.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 6.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.2f;
	KeyFrame[num_key].yPelota = 0.85f;
	KeyFrame[num_key].zPelota = 2.5f;
	KeyFrame[num_key].giroPlayer = 90.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 3.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.24f;
	KeyFrame[num_key].yPelota = 0.80f;
	KeyFrame[num_key].zPelota = 3.5f;
	KeyFrame[num_key].giroPlayer = 90.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 3.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.24f;
	KeyFrame[num_key].yPelota = 0.76f;
	KeyFrame[num_key].zPelota = 4.79f;
	KeyFrame[num_key].giroPlayer = 165.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 3.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -1.0f;
	KeyFrame[num_key].yPelota = 1.5f;
	KeyFrame[num_key].zPelota = 3.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -2.0f;
	KeyFrame[num_key].yPelota = 2.0f;
	KeyFrame[num_key].zPelota = 2.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -3.0f;
	KeyFrame[num_key].yPelota = 2.5f;
	KeyFrame[num_key].zPelota = 1.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -4.0f;
	KeyFrame[num_key].yPelota = 2.0f;
	KeyFrame[num_key].zPelota = 0.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -5.0f;
	KeyFrame[num_key].yPelota = 1.5f;
	KeyFrame[num_key].zPelota = -0.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -6.0f;
	KeyFrame[num_key].yPelota = 1.0f;
	KeyFrame[num_key].zPelota = -1.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -7.0f;
	KeyFrame[num_key].yPelota = 0.5f;
	KeyFrame[num_key].zPelota = -2.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = -8.0f;
	KeyFrame[num_key].yPelota = 0.05f;
	KeyFrame[num_key].zPelota = -3.5f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;
	//Termina sengundo lanzamiento
	KeyFrame[num_key].xPelota = 0.18f;
	KeyFrame[num_key].yPelota = -1.5f;
	KeyFrame[num_key].zPelota = 1.0f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 3.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.18f;
	KeyFrame[num_key].yPelota = -1.5f;
	KeyFrame[num_key].zPelota = 1.0f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.18f;
	KeyFrame[num_key].yPelota = -1.5f;
	KeyFrame[num_key].zPelota = 1.0f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * -3.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.18f;
	KeyFrame[num_key].yPelota = -1.5f;
	KeyFrame[num_key].zPelota = 1.0f;
	KeyFrame[num_key].giroPlayer = 180.0f;
	KeyFrame[num_key].giroPlayery = angulo * 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 12.0f;
	KeyFrame[num_key].giroBra1 = angulo * 12.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.18f;
	KeyFrame[num_key].yPelota = -1.5f;
	KeyFrame[num_key].zPelota = 1.0f;
	KeyFrame[num_key].giroPlayer = angulo * 12.0f;
	KeyFrame[num_key].giroPlayery = angulo * 2.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 3.0f;
	KeyFrame[num_key].giroBra = angulo * 12.0f;
	KeyFrame[num_key].giroBra1 = angulo * 12.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 2.0f;
	KeyFrame[num_key].giroCHy = angulo * 0.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.18f;
	KeyFrame[num_key].yPelota = -1.5f;
	KeyFrame[num_key].zPelota = 1.0f;
	KeyFrame[num_key].giroPlayer = angulo * 24.0f;
	KeyFrame[num_key].giroPlayery = angulo * 2.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 3.0f;
	KeyFrame[num_key].giroBra = angulo * 12.0f;
	KeyFrame[num_key].giroBra1 = angulo * 12.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 2.0f;
	KeyFrame[num_key].giroCHy = angulo * 12.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.18f;
	KeyFrame[num_key].yPelota = -1.5f;
	KeyFrame[num_key].zPelota = 1.0f;
	KeyFrame[num_key].giroPlayer = angulo * 36.0f;
	KeyFrame[num_key].giroPlayery = angulo * 2.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 3.0f;
	KeyFrame[num_key].giroBra = angulo * 12.0f;
	KeyFrame[num_key].giroBra1 = angulo * 12.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 2.0f;
	KeyFrame[num_key].giroCHy = angulo * 24.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.18f;
	KeyFrame[num_key].yPelota = -1.5f;
	KeyFrame[num_key].zPelota = 1.0f;
	KeyFrame[num_key].giroPlayer = angulo * 36.0f;
	KeyFrame[num_key].giroPlayery = angulo * 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 3.0f;
	KeyFrame[num_key].giroBra = angulo * 12.0f;
	KeyFrame[num_key].giroBra1 = angulo * 12.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 24.0f;
	num_key++;

	KeyFrame[num_key].xPelota = 0.18f;
	KeyFrame[num_key].yPelota = -1.5f;
	KeyFrame[num_key].zPelota = 1.0f;
	KeyFrame[num_key].giroPlayer = angulo * 36.0f;
	KeyFrame[num_key].giroPlayery = angulo * 0.0f;
	KeyFrame[num_key].giroPier1 = angulo * 0.0f;
	KeyFrame[num_key].giroPier2 = angulo * 0.0f;
	KeyFrame[num_key].giroBra = angulo * 0.0f;
	KeyFrame[num_key].giroBra1 = angulo * 0.0f;
	KeyFrame[num_key].giroCab = angulo * 0.0f;
	KeyFrame[num_key].giroCH = angulo * 0.0f;
	KeyFrame[num_key].giroCHy = angulo * 24.0f;
	num_key++;

	//**************************** While ************************************
	while (!mainWindow.getShouldClose())
	{

		//**************************** Logica dia noche ************************************
		musica.PosCamara(camera.getCameraPosition(), camera.getCameraDirection());

		GLfloat now = glfwGetTime();
		deltaTime = now - lastTime;
		lastTime = now;
		adelante = adelante + deltaTime;

		//**************************** Logica dia noche ************************************
		if (int(now) % lapso == 0) {
			if (accion) {
				accion = false;
				switch (faseDia)
				{
				case 0:
					skybox.createSkybox(Daybreak);
					faseDia++;
					mainLight = DirectionalLight(1.0f, 1.0f, 1.0f,
						0.3f, 0.3f,
						0.0f, 0.0f, 1.0f);
					luminarias = true;
					break;
				case 1:
					skybox.createSkybox(Midday);
					faseDia++;
					mainLight = DirectionalLight(1.0f, 1.0f, 1.0f,
						0.5f, 0.5f,
						0.0f, 1.0f, 1.0f);
					luminarias = false;
					break;
				case 2:
					skybox.createSkybox(Sunset);
					faseDia++;
					mainLight = DirectionalLight(1.0f, 1.0f, 1.0f,
						0.3f, 0.3f,
						0.0f, 0.0f, -1.0f);
					luminarias = false;
					break;
				case 3:
					skybox.createSkybox(Evening);
					faseDia++;
					mainLight = DirectionalLight(1.0f, 1.0f, 1.0f,
						0.2f, 0.2f,
						0.0f, 0.0f, -1.0f);
					luminarias = true;
					break;
				case 4:
					skybox.createSkybox(Midnight);
					faseDia = 0;
					mainLight = DirectionalLight(1.0f, 1.0f, 1.0f,
						0.1f, 0.1f,
						0.0f, 1.0f, 0.0f);
					luminarias = true;
					break;
				}
			}
		}
		else {
			accion = true;
		}

		//**************************** Logica luces spotlight ************************************
		if (mainWindow.getluzLinterna() == 1.0f)
		{
			//linterna
			spotLights[0] = SpotLight(1.0f, 1.0f, 1.0f,
				0.0f, 2.0f,
				0.0f, 0.0f, 0.0f,
				0.0f, -1.0f, 0.0f,
				1.0f, 0.0f, 0.0f,
				20.0f);

			glm::vec3 lowerLight = camera.getCameraPosition();
			lowerLight.y -= 0.3f;
			spotLights[0].SetFlash(lowerLight, camera.getCameraDirection());

			spotLightCount = 1;

			if (luminarias)
			{
				//Lampara 1
				pointLights[0] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					12.4f, 3.0f, 5.75f,
					0.3f, 0.2f, 0.1f);

				//Lampara 2
				pointLights[1] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					-13.5f, 3.0f, 7.0f,
					0.3f, 0.2f, 0.1f);

				//Lampara 3
				pointLights[2] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					-12.0f, 3.0f, -4.9f,
					0.3f, 0.2f, 0.1f);

				//Lampara 4
				pointLights[3] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					11.4f, 3.0f, -4.9f,
					0.3f, 0.2f, 0.1f);

				//Lampara 5
				pointLights[4] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					5.7f, 3.0f, 12.3f,
					0.3f, 0.2f, 0.1f);

				//Lampara 6
				pointLights[5] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					5.4f, 3.0f, -12.3f,
					0.3f, 0.2f, 0.1f);

				//Lampara 7
				pointLights[6] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					-6.0f, 3.0f, -12.1f,
					0.3f, 0.2f, 0.1f);

				//Lampara 8
				pointLights[7] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					-5.8f, 3.0f, 12.7f,
					0.3f, 0.2f, 0.1f);

				//Lampara 9
				pointLights[8] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					-20.5f, 4.0f, 20.5f,
					0.3f, 0.2f, 0.1f);

				//Lampara 10
				pointLights[9] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					20.5f, 4.0f, 20.5f,
					0.3f, 0.2f, 0.1f);

				//Lampara 11
				pointLights[10] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					-20.0f, 4.0f, -20.0f,
					0.3f, 0.2f, 0.1f);

				//Lampara 12
				pointLights[11] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					19.5f, 4.0f, -20.0f,
					0.3f, 0.2f, 0.1f);

				//Lampara 13
				pointLights[12] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					0.0f, 4.0f, -21.0f,
					0.3f, 0.2f, 0.1f);

				//Lampara 14
				pointLights[13] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					0.0f, 4.0f, 21.0f,
					0.3f, 0.2f, 0.1f);

				//luz ba�o 1
				spotLights[1] = SpotLight(1.0f, 1.0f, 1.0f,
					0.0f, 2.0f,
					21.0f, 2.5f, 0.0f,
					0.0f, -1.0f, 0.0f,
					1.0f, 0.0f, 0.0f,
					60.0f);

				//luz ba�o 2
				spotLights[2] = SpotLight(1.0f, 1.0f, 1.0f,
					0.0f, 2.0f,
					-21.0f, 2.5f, 0.0f,
					0.0f, -1.0f, 0.0f,
					1.0f, 0.0f, 0.0f,
					60.0f);

				pointLightCount = 14;

				spotLightCount = 3;
			}
		}
		else {
			spotLightCount = 0;
			pointLightCount = 0;

			if (luminarias)
			{
				//Lampara 1
				pointLights[0] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					12.4f, 3.0f, 5.75f,
					0.3f, 0.2f, 0.1f);

				//Lampara 2
				pointLights[1] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					-13.5f, 3.0f, 7.0f,
					0.3f, 0.2f, 0.1f);

				//Lampara 3
				pointLights[2] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					-12.0f, 3.0f, -4.9f,
					0.3f, 0.2f, 0.1f);

				//Lampara 4
				pointLights[3] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					11.4f, 3.0f, -4.9f,
					0.3f, 0.2f, 0.1f);

				//Lampara 5
				pointLights[4] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					5.7f, 3.0f, 12.3f,
					0.3f, 0.2f, 0.1f);

				//Lampara 6
				pointLights[5] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					5.4f, 3.0f, -12.3f,
					0.3f, 0.2f, 0.1f);

				//Lampara 7
				pointLights[6] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					-6.0f, 3.0f, -12.1f,
					0.3f, 0.2f, 0.1f);

				//Lampara 8
				pointLights[7] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					-5.8f, 3.0f, 12.7f,
					0.3f, 0.2f, 0.1f);

				//Lampara 9
				pointLights[8] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					-20.5f, 4.0f, 20.5f,
					0.3f, 0.2f, 0.1f);

				//Lampara 10
				pointLights[9] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					20.5f, 4.0f, 20.5f,
					0.3f, 0.2f, 0.1f);

				//Lampara 11
				pointLights[10] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					-20.0f, 4.0f, -20.0f,
					0.3f, 0.2f, 0.1f);

				//Lampara 12
				pointLights[11] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					19.5f, 4.0f, -20.0f,
					0.3f, 0.2f, 0.1f);

				//Lampara 13
				pointLights[12] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					0.0f, 4.0f, -21.0f,
					0.3f, 0.2f, 0.1f);

				//Lampara 14
				pointLights[13] = PointLight(1.0f, 1.0f, 1.0f,
					0.6f, 1.0f,
					0.0f, 4.0f, 21.0f,
					0.3f, 0.2f, 0.1f);

				//luz ba�o 1
				spotLights[0] = SpotLight(1.0f, 1.0f, 1.0f,
					0.0f, 2.0f,
					21.0f, 2.5f, 0.0f,
					0.0f, -1.0f, 0.0f,
					1.0f, 0.0f, 0.0f,
					60.0f);

				//luz ba�o 2
				spotLights[1] = SpotLight(1.0f, 1.0f, 1.0f,
					0.0f, 2.0f,
					-21.0f, 2.5f, 0.0f,
					0.0f, -1.0f, 0.0f,
					1.0f, 0.0f, 0.0f,
					60.0f);

				pointLightCount = 14;

				spotLightCount = 2;
			}
		}

		//**************************** Luces Kiosko ************************************

		//Animaci�n de luces
		if (mainWindow.getAnimacionKiosko() && mainWindow.getLucesKiosko()) {

			//Musica
			musica.SetMusicaFiesta();

			if (lapsoLuces == 0)
			{
				lapsoLuces = int(now);
			}
			else {
				if (int(now) - lapsoLuces < 10)
				{
					if (indiceLuces == 0)
					{
						movimientoVertical += deltaTime;
						if (movimientoVertical > 2.0f)
						{
							indiceLuces = 1;
						}
					}
					else if (indiceLuces == 1)
					{
						movimientoVertical -= deltaTime;
						if (movimientoVertical < 0.0f)
						{
							indiceLuces = 0;
						}
					}
				}
				else if (int(now) - lapsoLuces < 25)
				{
					if (indiceLuces == 0)
					{
						movimientoVertical += deltaTime;
						movimientoHorizontal -= deltaTime;
						if (movimientoVertical > 1.5f)
						{
							indiceLuces = 1;
						}
					}
					else if (indiceLuces == 1)
					{
						movimientoVertical -= deltaTime;
						movimientoHorizontal += deltaTime;
						if (movimientoVertical < 0.0f)
						{
							indiceLuces = 2;
						}
					}
					else if (indiceLuces == 2)
					{
						movimientoVertical += deltaTime;
						movimientoHorizontal -= deltaTime;
						if (movimientoVertical > 1.5f)
						{
							indiceLuces = 3;
						}
					}
					else if (indiceLuces == 3)
					{
						movimientoVertical -= deltaTime;
						movimientoHorizontal -= deltaTime;
						if (movimientoVertical < 0.0f)
						{
							indiceLuces = 4;
						}
					}
					else if (indiceLuces == 4)
					{
						movimientoVertical += deltaTime;
						movimientoHorizontal += deltaTime;
						if (movimientoVertical > 1.5f)
						{
							indiceLuces = 0;
						}
					}
				}
				else {
					if (movimientoVertical > 0.0f)
					{
						movimientoVertical -= deltaTime;
					}
					else {
						movimientoVertical = 0;
					}
					if (movimientoHorizontal > 0.0f)
					{
						movimientoHorizontal -= deltaTime;
					}
					else {
						movimientoHorizontal = 0;
					}
				}

				if (int(now) - lapsoLuces < 25)
				{
					if (color)
					{
						if (luces < 1.0)
						{
							luces += deltaTime;
						}
						else {
							color = false;
						}
					}
					else {
						if (luces > 0.0)
						{
							luces -= deltaTime;
						}
						else {
							color = true;
						}
					}
				}

				if (int(now) - lapsoLuces > 26)
				{
					indiceLuces = 0;
					movimientoVertical = 0;
					movimientoHorizontal = 0;
					lapsoLuces = 0;
					luces = 1.0f;
				}

				spotLights[spotLightCount] = SpotLight(luces, luces, luces,
					0.0f, 1.0f,
					0.0f, 2.0f, 0.0f,
					0.0f, -1.0f, 0.0f,
					1.0f, 0.0f, 0.0f,
					15.0f);
				spotLightCount++;

				spotLights[spotLightCount] = SpotLight(1.0f, 0.0f, 0.0f,
					0.0f, 1.0f,
					0.0f, 1.35f, 1.5f,
					0.0f + movimientoHorizontal, -1.0f, -1.0f + movimientoVertical,
					1.0f, 0.0f, 0.0f,
					30.0f);
				spotLightCount++;

				spotLights[spotLightCount] = SpotLight(0.0f, 1.0f, 0.0f,
					0.0f, 1.0f,
					0.0f, 1.35f, -1.5f,
					0.0f - movimientoHorizontal, -1.0f, 1.0f - movimientoVertical,
					1.0f, 0.0f, 0.0f,
					30.0f);
				spotLightCount++;

				spotLights[spotLightCount] = SpotLight(0.0f, 0.0f, 1.0f,
					0.0f, 1.0f,
					1.5f, 1.35f, 0.0f,
					-1.0f + movimientoVertical, -1.0f, 0.0f + movimientoHorizontal,
					1.0f, 0.0f, 0.0f,
					30.0f);
				spotLightCount++;

				spotLights[spotLightCount] = SpotLight(1.0f, 0.0f, 1.0f,
					0.0f, 1.0f,
					-1.5f, 1.35f, 0.0f,
					1.0f - movimientoVertical, -1.0f, 0.0f - movimientoHorizontal,
					1.0f, 0.0f, 0.0f,
					30.0f);
				spotLightCount++;
			}
		}
		//Reiniciar variables
		else {
			indiceLuces = 0;
			movimientoVertical = 0;
			movimientoHorizontal = 0;
			lapsoLuces = 0;
			luces = 1.0f;
			//Musica
			musica.SetMusicaFondo();
		}

		//Encender luces
		if (mainWindow.getLucesKiosko() && !mainWindow.getAnimacionKiosko())
		{
			lapsoLuces = 0;

			spotLights[spotLightCount] = SpotLight(1.0f, 1.0f, 1.0f,
				0.0f, 1.0f,
				0.0f, 2.0f, 0.0f,
				0.0f, -1.0f, 0.0f,
				1.0f, 0.0f, 0.0f,
				15.0f);
			spotLightCount++;

			spotLights[spotLightCount] = SpotLight(1.0f, 0.0f, 0.0f,
				0.0f, 1.0f,
				0.0f, 1.35f, 1.5f,
				0.0f, -1.0f, -1.0f,
				1.0f, 0.0f, 0.0f,
				30.0f);
			spotLightCount++;

			spotLights[spotLightCount] = SpotLight(0.0f, 1.0f, 0.0f,
				0.0f, 1.0f,
				0.0f, 1.35f, -1.5f,
				0.0f, -1.0f, 1.0f,
				1.0f, 0.0f, 0.0f,
				30.0f);
			spotLightCount++;

			spotLights[spotLightCount] = SpotLight(0.0f, 0.0f, 1.0f,
				0.0f, 1.0f,
				1.5f, 1.35f, 0.0f,
				-1.0f, -1.0f, 0.0f,
				1.0f, 0.0f, 0.0f,
				30.0f);
			spotLightCount++;

			spotLights[spotLightCount] = SpotLight(1.0f, 0.0f, 1.0f,
				0.0f, 1.0f,
				-1.5f, 1.35f, 0.0f,
				1.0f, -1.0f, 0.0f,
				1.0f, 0.0f, 0.0f,
				30.0f);
			spotLightCount++;
		}

		//**************************** Animaci�n simple ************************************

		//Juegos
		if (mainWindow.getAnimJuegos())
		{
			//Columpio
			if (angColumpio > 30.0f || angColumpio < -30.0f)
			{
				swing = !swing;
			}
			if (swing)
			{
				angColumpio += juegosOffset;
			}
			if (!swing)
			{
				angColumpio -= juegosOffset;
			}


			//Rueda
			angRueda += juegosOffset;
			if (angRueda >= 360.0f)
			{
				angRueda = 0.0f;
			}
		}

		//Baile
		if (mainWindow.getAnimBaile())
		{
			angPer += perOffset;
			if (angPer >= 360.0f)
			{
				angPer = 0.0f;
			}

			if (edo1)
			{
				if (angExt > -60.0f)
				{
					angExt -= extremidadesOffset;
				}
				else {
					edo1 = false;
					edo2 = true;
				}
			}

			if (edo2)
			{
				if (angExt < 0.0f)
				{
					angExt += extremidadesOffset;
				}
				else {
					edo2 = false;
					edo3 = true;
				}
			}

			if (edo3)
			{
				if (angExt2 > -60.0f)
				{
					angExt2 -= extremidadesOffset;
				}
				else {
					edo3 = false;
					edo4 = true;
				}
			}

			if (edo4)
			{
				if (angExt2 < 0.0f)
				{
					angExt2 += extremidadesOffset;
				}
				else {
					edo4 = false;
					edo1 = true;
				}
			}
		}
		else {
			angPer = 0.0f;
			angExt = 0.0f;
			angExt2 = 0.0f;
			edo1 = true;
			edo2 = false;
			edo3 = false;
			edo4 = false;

		}

		//**************************** Animaci�n compleja ************************************
		if (mainWindow.getAnimSnoopy())
		{
			if (theta < 360.0f)
			{
				movxS = 10.0f * cos(theta * toRadians);
				movzS = 10.0f * sin(theta * toRadians);
				movyS = sin(modAltura * theta * toRadians) + 4.0f;
				rotaSnoopy += rotaOffset;
				theta += angOffset;

			}
			else
			{
				theta = 0.0f;
				if (modAltura == 5.0f)
				{
					modAltura = 10.0f;
				}
				else {
					modAltura = 5.0f;
				}
			}
		}

		//Recibir eventos del usuario
		glfwPollEvents();

		camera.keyControl(mainWindow.getsKeys(), deltaTime);
		camera.mouseControl(mainWindow.getXChange(), mainWindow.getYChange());

		//para keyframes
		inputKeyframes(mainWindow.getsKeys());
		animate();

		// Clear the window
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		skybox.DrawSkybox(camera.calculateViewMatrix(), projection, adelante);
		shaderList[0].UseShader();
		uniformModel = shaderList[0].GetModelLocation();
		uniformProjection = shaderList[0].GetProjectionLocation();
		uniformView = shaderList[0].GetViewLocation();
		uniformEyePosition = shaderList[0].GetEyePositionLocation();
		uniformSpecularIntensity = shaderList[0].GetSpecularIntensityLocation();
		uniformShininess = shaderList[0].GetShininessLocation();
		uniformColor = shaderList[0].getColorLocation();

		shaderList[0].SetDirectionalLight(&mainLight);
		shaderList[0].SetPointLights(pointLights, pointLightCount);
		shaderList[0].SetSpotLights(spotLights, spotLightCount);

		glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(uniformView, 1, GL_FALSE, glm::value_ptr(camera.calculateViewMatrix()));
		glUniform3f(uniformEyePosition, camera.getCameraPosition().x, camera.getCameraPosition().y, camera.getCameraPosition().z);

		//Inicializaci�n de color
		glm::vec3 colorMaterial = glm::vec3(1.0f, 1.0f, 1.0f);
		glUniform3fv(uniformColor, 1, glm::value_ptr(colorMaterial));

		//****************************** Inicio carga de modelos ************************

		glm::mat4 model(1.0);
		glm::mat4 modelaux(1.0);
		glm::mat4 modelaux2(1.0);
		glm::mat4 modelaux3(1.0);
		glm::mat4 modelaux4(1.0);
		glm::mat4 modelAux(1.0);

		//Piso
		model = glm::mat4(1.0);
		modelaux3 = model;
		model = glm::scale(model, glm::vec3(2.5f, 2.5f, 2.5f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		plainTexture.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[2]->RenderMesh();

		// ******************** Charly ***********************************

		//Base
		model = glm::mat4(1.0);
		//model = glm::translate(model, glm::vec3(0.0f + mainWindow.getx(), 0.25f, 0.0f + mainWindow.getz()));
		model = glm::translate(model, glm::vec3(0.0f, 0.55f, 1.0f));
		model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
		model = glm::rotate(model, angPer * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::rotate(model, giroCHy * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::rotate(model, -giroCH * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		modelaux = model;
		modelaux2 = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Base_M.RenderModel();

		//Rueda
		model = modelaux;
		model = glm::translate(model, glm::vec3(0.0f, -5.8f, 0.0f));
		modelaux = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Rueda_M.RenderModel();

		//Piernas
		model = modelaux;
		model = glm::translate(model, glm::vec3(-5.85f, 0.0f, 0.0f));
		model = glm::rotate(model, angExt * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, -giroPier2 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PieDer_M.RenderModel();

		model = modelaux;
		model = glm::translate(model, glm::vec3(6.0f, -0.4f, 0.2f));
		model = glm::rotate(model, angExt2 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PieIzq_M.RenderModel();

		//Torzo
		model = modelaux2;
		model = glm::translate(model, glm::vec3(0.0f, 11.95f, 0.0f));
		modelaux = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		CharlyTorzo_M.RenderModel();

		//Cabeza
		model = modelaux;
		model = glm::translate(model, glm::vec3(0.0f, 20.0f, 0.0f));
		model = glm::rotate(model, giroCab * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		CharlyHead_M.RenderModel();

		//Brazo derecho
		model = modelaux;
		model = glm::translate(model, glm::vec3(-10.87f, 6.0f, 0.0f));
		model = glm::rotate(model, angExt2 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, -giroBra1 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		modelaux2 = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		CharlyBraDer_M.RenderModel();

		//Mano derecha
		model = modelaux2;
		model = glm::translate(model, glm::vec3(-4.43f, -12.0f, -2.18f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		ManDer_M.RenderModel();

		//Brazo izquierdo
		model = modelaux;
		model = glm::translate(model, glm::vec3(11.2f, 6.8f, 0.0f));
		model = glm::rotate(model, angExt * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, -giroBra * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		modelaux2 = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		CharlyBraIzq_M.RenderModel();

		//Mano izquierda
		model = modelaux2;
		model = glm::translate(model, glm::vec3(3.4f, -12.0f, -2.65f));
		//model = glm::rotate(model, mainWindow.getrotateManIzq() * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		ManIzq_M.RenderModel();

		// ******************** Player ***********************************

		//Base
		model = glm::mat4(1.0);
		model = glm::translate(model, glm::vec3(0.0f, 0.24f, 5.0f));
		model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
		model = glm::rotate(model, giroPlayer * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::rotate(model, -giroPlayery * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		modelaux = model;
		modelaux2 = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Base_M.RenderModel();

		//Rueda
		model = modelaux;
		model = glm::translate(model, glm::vec3(0.0f, -5.8f, 0.0f));
		modelaux = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Rueda_M.RenderModel();

		//Piernas
		model = modelaux;
		model = glm::translate(model, glm::vec3(-5.85f, 0.0f, 0.0f));
		model = glm::rotate(model, -giroPier1 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PieDer_M.RenderModel();

		model = modelaux;
		model = glm::translate(model, glm::vec3(6.0f, -0.4f, 0.2f));
		model = glm::rotate(model, -giroPier2 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PieIzq_M.RenderModel();

		//Torzo
		model = modelaux2;
		model = glm::translate(model, glm::vec3(0.0f, 11.95f, 0.0f));
		modelaux = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PlayerTorzo_M.RenderModel();

		//Cabeza
		model = modelaux;
		model = glm::translate(model, glm::vec3(0.0f, 20.0f, 0.0f));
		model = glm::rotate(model, -giroCab * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PlayerHead_M.RenderModel();

		//Brazo derecho
		model = modelaux;
		model = glm::translate(model, glm::vec3(-10.87f, 6.0f, 0.0f));
		model = glm::rotate(model, 58.0f * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::rotate(model, -70 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		modelaux2 = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PlayerBraDer_M.RenderModel();

		//Mano derecha
		model = modelaux2;
		model = glm::translate(model, glm::vec3(-4.43f, -12.0f, -2.18f));
		model = glm::rotate(model, -34.0f * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, -30.0f * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::rotate(model, 13.0f * toRadians, glm::vec3(0.0f, 0.0f, 1.0f));
		modelAux = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		ManDer_M.RenderModel();

		//Bate
		model = modelAux;
		model = glm::translate(model, glm::vec3(-1.43f, -4.4f, -0.82f));
		model = glm::rotate(model, 87.0f * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, -8.0f * toRadians, glm::vec3(0.0f, 0.0f, 1.0f));
		model = glm::scale(model, glm::vec3(0.015f, 0.015f, 0.015f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Bate_M.RenderModel();

		//Brazo izquierdo
		model = modelaux;
		model = glm::translate(model, glm::vec3(11.2f, 6.8f, 0.0f));
		model = glm::rotate(model, -58.0f * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::rotate(model, -50 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		modelaux2 = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PlayerBraIzq_M.RenderModel();

		//Mano izquierda
		model = modelaux2;
		model = glm::translate(model, glm::vec3(3.4f, -12.0f, -2.65f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		ManIzq_M.RenderModel();

		//********************** Pelota ***********************************
		model = modelaux3;
		//model = glm::translate(model, glm::vec3(0.17f + mainWindow.getx(), 0.93f + mainWindow.gety(), 1.14f + mainWindow.getz()));
		model = glm::translate(model, glm::vec3(xPelota, yPelota, zPelota));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Pelota.render();


		//************************************** Linus *******************************

		glm::vec3 posicionPersonaje= camera.getPersonajePosition();

		//Base
		model = glm::mat4(1.0);

		model = glm::translate(
			model,
			glm::vec3(posicionPersonaje.x, 0.24f, posicionPersonaje.z)
		);

		model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
		model = glm::rotate(model, camera.getAnguloPersonaje() * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		
		modelaux = model;
		modelaux2 = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Base_M.RenderModel();

		//Rueda
		model = modelaux;
		model = glm::translate(model, glm::vec3(0.0f, -5.8f, 0.0f));
		modelaux = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Rueda_M.RenderModel();

		//Piernas
		model = modelaux;
		model = glm::translate(model, glm::vec3(-5.85f, 0.0f, 0.0f));
		model = glm::rotate(model, angExt2 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PieDer_M.RenderModel();

		model = modelaux;
		model = glm::translate(model, glm::vec3(6.0f, -0.4f, 0.2f));
		model = glm::rotate(model, angExt * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PieIzq_M.RenderModel();

		//Torzo
		model = modelaux2;
		model = glm::translate(model, glm::vec3(0.0f, 11.95f, 0.0f));
		modelaux = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		LinusTorzo_M.RenderModel();

		//Cabeza
		model = modelaux;
		model = glm::translate(model, glm::vec3(0.0f, 20.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		LinusHead_M.RenderModel();

		//Brazo derecho
		model = modelaux;
		model = glm::translate(model, glm::vec3(-10.87f, 6.0f, 0.0f));
		model = glm::rotate(model, angExt * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		modelaux2 = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		LinusBraDer_M.RenderModel();

		//Mano derecha
		model = modelaux2;
		model = glm::translate(model, glm::vec3(-4.36f, -12.0f, -2.5f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		ManDer_M.RenderModel();

		//Brazo izquierdo
		model = modelaux;
		model = glm::translate(model, glm::vec3(11.2f, 6.8f, 0.0f));
		model = glm::rotate(model, angExt2 * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		modelaux2 = model;
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		LinusBraIzq_M.RenderModel();

		//Mano izquierda
		model = modelaux2;
		model = glm::translate(model, glm::vec3(3.5f, -12.0f, -2.9f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		ManIzq_M.RenderModel();
		
		//********************** Postes ******************************
		model = modelaux3;
		model = glm::translate(model, glm::vec3(0.0f, 0.88f, 0.0f));
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Postes_M.RenderModel();

		//Poste Fuente 1
		model = modelaux3;
		model = glm::translate(model, glm::vec3(0.0f, 2.089f, 22.23f));
		model = glm::rotate(model, 180 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.0008, 0.0008f, 0.0008));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PosteL_M.RenderModel();

		//Poste Fuente 2
		model = modelaux3;
		model = glm::translate(model, glm::vec3(0.0f, 2.089f, -22.23f));
		model = glm::scale(model, glm::vec3(0.0008, 0.0008f, 0.0008));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PosteL_M.RenderModel();

		//Poste juegos
		model = modelaux3;
		model = glm::translate(model, glm::vec3(19.27f, 2.089f, -21.23f));
		model = glm::scale(model, glm::vec3(0.0008, 0.0008f, 0.0008));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PosteL_M.RenderModel();

		//Bancas 1
		model = modelaux3;
		model = glm::translate(model, glm::vec3(-21.34f, 2.089f, -21.34f));
		model = glm::scale(model, glm::vec3(0.0008, 0.0008f, 0.0008));
		model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PosteL_M.RenderModel();

		//Bancas 2
		model = modelaux3;
		model = glm::translate(model, glm::vec3(21.69f, 2.089f, 21.69f));
		model = glm::scale(model, glm::vec3(0.0008, 0.0008f, 0.0008));
		model = glm::rotate(model, -135 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PosteL_M.RenderModel();

		//Tren luz
		model = modelaux3;
		model = glm::translate(model, glm::vec3(-21.37f + mainWindow.gety(), 2.089f, 21.31f + mainWindow.gety()));
		model = glm::scale(model, glm::vec3(0.0008, 0.0008f, 0.0008));
		model = glm::rotate(model, 135 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		PosteL_M.RenderModel();


		//********************** Basura ******************************
		model = modelaux3;
		model = glm::translate(model, glm::vec3(0.43f, 0.26f, -0.69f));
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		colorMaterial = glm::vec3(0.2f, 0.2f, 0.2f);
		glUniform3fv(uniformColor, 1, glm::value_ptr(colorMaterial));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Trash_M.RenderModel();
		colorMaterial = glm::vec3(1.0f, 1.0f, 1.0f);
		glUniform3fv(uniformColor, 1, glm::value_ptr(colorMaterial));


		//********************** Rejas ******************************
		model = modelaux3;
		model = glm::translate(model, glm::vec3(0.0f, 0.24f, 0.0f));
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Rejas_M.RenderModel();

		//********************** Plantas ******************************
		model = modelaux3;
		model = glm::translate(model, glm::vec3(0.0f, 1.21f, 0.0f));
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Plantas_M.RenderModel();

		//************************ Tren **************************************
		model = modelaux3;
		model = glm::translate(model, glm::vec3(-19.19f, 0.84f, 19.19f));
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		model = glm::rotate(model, 60 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Tren_M.RenderModel();

		//************************ Juegos y bancas **************************************
		model = modelaux3;
		model = glm::translate(model, glm::vec3(0.0f, 1.19f, 0.0f));
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		JB_M.RenderModel();

		//Columpio
		model = modelaux3;
		model = glm::translate(model, glm::vec3(17.75f, 1.23f, -21.23f));
		modelAux = model;
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		BaseColumpio_M.RenderModel();

		model = modelAux;
		//model = glm::translate(model, glm::vec3(17.75f, 2.099f, -21.23f));
		model = glm::translate(model, glm::vec3(0.0f, 0.869f, 0.0f));
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		model = glm::rotate(model, angColumpio * toRadians, glm::vec3(1.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Columpio_M.RenderModel();

		//Rueda
		model = modelaux3;
		model = glm::translate(model, glm::vec3(19.27f, 0.21f, -17.9f));
		modelAux = model;
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		JBaseRueda_M.RenderModel();

		model = modelAux;
		//model = glm::translate(model, glm::vec3(19.27f, 0.42f, -17.9f));
		model = glm::translate(model, glm::vec3(0.0f, 0.21f, 0.0f));
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		model = glm::rotate(model, angRueda * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		JRueda_M.RenderModel();

		//********************** Fuentes ***********************************
		model = modelaux3;
		model = glm::translate(model, glm::vec3(0.0f, 1.08f, 0.0f));
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Fuentes_M.RenderModel();

		//************************ Ba�os **************************************
		model = modelaux3;
		model = glm::translate(model, glm::vec3(21.21f, 1.14f, 0.0f));
		modelAux = model;
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		model = glm::rotate(model, -180 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Bath1_M.RenderModel();

		model = modelAux;
		//model = glm::translate(model, glm::vec3(21.21f, 2.078f + mainWindow.getx(), 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, 0.938f, 0.0f));
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Foco_M.RenderModel();

		model = modelaux3;
		model = glm::translate(model, glm::vec3(-21.21f, 1.14f, 0.0f));
		modelAux = model;
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		//model = glm::rotate(model, -180 + mainWindow.getrotateBraDer() * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Bath2_M.RenderModel();

		model = modelAux;
		//model = glm::translate(model, glm::vec3(-21.21f, 2.078f + mainWindow.getx(), 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, 0.938f, 0.0f));
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Foco_M.RenderModel();


		//********************** Snoopy volando ***********************************
		model = modelaux3;
		model = glm::translate(model, glm::vec3(movxS, movyS, movzS));
		model = glm::scale(model, glm::vec3(0.00036, 0.00036f, 0.00036));
		model = glm::rotate(model, -rotaSnoopy * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		Snoopy_M.RenderModel();


		//********************** Kiosko *******************************

		//Base 1
		model = glm::mat4(1.0);
		model = glm::scale(model, glm::vec3(0.15f, 0.15f, 0.15f));
		model = glm::translate(model, glm::vec3(0.0f, -0.6f, 0.0f));

		model = glm::translate(model, glm::vec3(0.0f, 1.0f, 0.0f));
		modelAux = model;
		model = glm::scale(model, glm::vec3(1.6f, 0.25f, 1.6f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		cafe.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[3]->RenderMesh();

		//Base 2
		model = modelAux;
		model = glm::translate(model, glm::vec3(0.0f, 0.5f, 0.0f));
		modelAux = model;
		model = glm::scale(model, glm::vec3(1.4f, 0.25f, 1.4f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		cafe.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[3]->RenderMesh();

		//Base 3
		glm::mat4 modelBase(1.0);
		model = modelAux;
		model = glm::translate(model, glm::vec3(0.0f, 0.5f, 0.0f));
		modelBase = model;
		modelAux = model;
		model = glm::scale(model, glm::vec3(1.2f, 0.25f, 1.2f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		cafe.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[3]->RenderMesh();


		//Pilar 1-1
		glm::mat4 modelPilar(1.0);
		modelPilar = modelBase;
		model = modelPilar;
		model = glm::translate(model, glm::vec3(-4.0f, 0.15f, 10.3f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 1-2
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.4f, 6.0f, 0.4f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		madera.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 1-3
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 2-1
		modelPilar = modelBase;
		model = modelPilar;
		model = glm::translate(model, glm::vec3(4.0f, 0.15f, 10.3f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 2-2
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.4f, 6.0f, 0.4f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		madera.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 2-3
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 3-1
		modelPilar = modelBase;
		model = modelPilar;
		model = glm::translate(model, glm::vec3(-4.0f, 0.15f, -10.3f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 3-2
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.4f, 6.0f, 0.4f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		madera.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 3-3
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 4-1
		modelPilar = modelBase;
		model = modelPilar;
		model = glm::translate(model, glm::vec3(4.0f, 0.15f, -10.3f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 4-2
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.4f, 6.0f, 0.4f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		madera.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 4-3
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 5-1
		modelPilar = modelBase;
		model = modelPilar;
		model = glm::translate(model, glm::vec3(-10.3f, 0.15f, -4.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 5-2
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.4f, 6.0f, 0.4f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		madera.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 5-3
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 6-1
		modelPilar = modelBase;
		model = modelPilar;
		model = glm::translate(model, glm::vec3(-10.3f, 0.15f, 4.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 6-2
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.4f, 6.0f, 0.4f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		madera.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 6-3
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 7-1
		modelPilar = modelBase;
		model = modelPilar;
		model = glm::translate(model, glm::vec3(10.3f, 0.15f, -4.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 7-2
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.4f, 6.0f, 0.4f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		madera.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 7-3
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 8-1
		modelPilar = modelBase;
		model = modelPilar;
		model = glm::translate(model, glm::vec3(10.3f, 0.15f, 4.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 8-2
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.4f, 6.0f, 0.4f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		madera.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Pilar 8-3
		model = modelPilar;
		model = glm::translate(model, glm::vec3(0.0f, 3.4f, 0.0f));
		modelPilar = model;
		model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		oro.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[4]->RenderMesh();

		//Barandal 2
		model = modelBase;
		model = glm::translate(model, glm::vec3(-7.15f, -0.3f, 7.15f));
		model = glm::rotate(model, -45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.08f, 0.05f, 0.08f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		barandal.RenderModel();

		//Barandal 3
		model = modelBase;
		model = glm::translate(model, glm::vec3(-10.3f, -0.3f, 0.0f));
		model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.08f, 0.05f, 0.08f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		barandal.RenderModel();

		//Barandal 4
		model = modelBase;
		model = glm::translate(model, glm::vec3(-7.15f, -0.3f, -7.15f));
		model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.08f, 0.05f, 0.08f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		barandal.RenderModel();

		//Barandal 5
		model = modelBase;
		model = glm::translate(model, glm::vec3(0.0f, -0.3f, -10.3f));
		model = glm::scale(model, glm::vec3(0.08f, 0.05f, 0.08f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		barandal.RenderModel();

		//Barandal 6
		model = modelBase;
		model = glm::translate(model, glm::vec3(7.15f, -0.3f, -7.15f));
		model = glm::rotate(model, -45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.08f, 0.05f, 0.08f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		barandal.RenderModel();

		//Barandal 7
		model = modelBase;
		model = glm::translate(model, glm::vec3(10.3f, -0.3f, 0.0f));
		model = glm::rotate(model, 90 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.08f, 0.05f, 0.08f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		barandal.RenderModel();

		//Barandal 8
		model = modelBase;
		model = glm::translate(model, glm::vec3(7.15f, -0.3f, 7.15f));
		model = glm::rotate(model, 45 * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.08f, 0.05f, 0.08f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		barandal.RenderModel();

		//Base 4
		model = modelAux;
		model = glm::translate(model, glm::vec3(0.0f, 0.5f, 0.0f));
		modelAux = model;
		model = glm::scale(model, glm::vec3(1.0f, 0.25f, 1.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		magenta.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[3]->RenderMesh();

		//Techo 1
		model = modelAux;
		model = glm::translate(model, glm::vec3(0.0f, 7.1f, 0.0f));
		modelAux = model;
		model = glm::scale(model, glm::vec3(1.6f, 0.7f, 1.6f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		cafe.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[3]->RenderMesh();

		//Candelabro
		model = modelAux;
		model = glm::translate(model, glm::vec3(0.0f, -4.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		candelabro.RenderModel();

		//Domo
		model = modelAux;
		model = glm::translate(model, glm::vec3(0.0f, 4.2f, 0.0f));
		model = glm::rotate(model, 22.5f * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_opaco.UseMaterial(uniformSpecularIntensity, uniformShininess);
		domo.RenderModel();

		//Techo 2
		model = modelAux;
		model = glm::translate(model, glm::vec3(0.0f, -1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(1.2f, 0.8f, 1.2f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		magenta.UseTexture();
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		meshList[5]->RenderMesh();

		//Lampara 1
		model = modelAux;
		model = glm::translate(model, glm::vec3(-10.0f, -0.45f, 0.0f));
		model = glm::scale(model, glm::vec3(0.07f, 0.07f, 0.07f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		lampara.RenderModel();

		//Lampara 2
		model = modelAux;
		model = glm::translate(model, glm::vec3(10.0f, -0.45f, 0.0f));
		model = glm::rotate(model, 180.0f * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.07f, 0.07f, 0.07f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		lampara.RenderModel();

		//Lampara 3
		model = modelAux;
		model = glm::translate(model, glm::vec3(0.0f, -0.45f, 10.0f));
		model = glm::rotate(model, 90.0f * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.07f, 0.07f, 0.07f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		lampara.RenderModel();

		//Lampara 4
		model = modelAux;
		model = glm::translate(model, glm::vec3(0.0f, -0.45f, -10.0f));
		model = glm::rotate(model, -90.0f * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.07f, 0.07f, 0.07f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		Material_brillante.UseMaterial(uniformSpecularIntensity, uniformShininess);
		lampara.RenderModel();
		
		mainWindow.swapBuffers();
	}

	musica.Drop();
	return 0;
}

void inputKeyframes(bool* keys)
{
	if (keys[GLFW_KEY_SPACE])
	{
		if (reproduciranimacion < 1)
		{
			if (play == false && (FrameIndex > 1))
			{
				resetElements();
				//First Interpolation				
				interpolation();
				play = true;
				playIndex = 0;
				i_curr_steps = 0;
				reproduciranimacion++;
				printf("presiona 0 para habilitar reproducir de nuevo la animaci�n'\n");
				habilitaranimacion = 0;
				musica.SetMusicaBaseball();
			}
			else
			{
				play = false;
			}
		}
	}
	if (keys[GLFW_KEY_0])
	{
		if (habilitaranimacion < 1)
		{
			reproduciranimacion = 0;
		}
	}

}