
#include <glm.hpp>
#include <gtc\matrix_transform.hpp>

#include<irrKlang/irrKlang.h>

class Musica
{
	public:
		Musica();
		int Initialise();
		void Drop();
		void SetMusicaFiesta();
		void SetMusicaFondo();
		void SetMusicaBaseball();
		void UnsetMusicaBaseball();
		void PosCamara(glm::vec3 pos, glm::vec3 normal);
		~Musica();

	private:
		irrklang::ISoundSource* source;
		irrklang::ISoundSource* musicaFondo;
		irrklang::ISoundSource* musicaFiesta;
		irrklang::ISoundSource* musicaBaseball;
		irrklang::ISoundEngine* engine;
		//Sonidos
		irrklang::ISound* bano1;
		irrklang::ISound* bano2;
		irrklang::ISound* charlieBrown;
		irrklang::ISound* fiesta;
		irrklang::ISound* fuente1;
		irrklang::ISound* fuente2;
		irrklang::ISound* juegos;
		irrklang::ISound* tren;
		irrklang::ISound* baseball;
};

