#include "Window.h"

Window::Window()
{
	width = 800;
	height = 600;
	for (size_t i = 0; i < 1024; i++)
	{
		keys[i] = 0;
	}
}
Window::Window(GLint windowWidth, GLint windowHeight)
{
	width = windowWidth;
	height = windowHeight;

	luzLinterna = 1.0f;
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
	//rotateBraDer = 0.0f;
	//rotateBraIzq = 0.0f;
	//rotateManDer = 0.0f;
	//rotateManIzq = 0.0f;
	//rotatePieDer = 0.0f;
	//rotatePieIzq = 0.0f;

	animJuegos = true;
	animBaile = false;
	animSnoopy = true;
	lucesKiosko = false;
	animacionKiosko = false;

	for (size_t i = 0; i < 1024; i++)
	{
		keys[i] = 0;
	}
}
int Window::Initialise()
{
	//Inicializaci�n de GLFW
	if (!glfwInit())
	{
		printf("Fall� inicializar GLFW");
		glfwTerminate();
		return 1;
	}
	//Asignando variables de GLFW y propiedades de ventana
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	//para solo usar el core profile de OpenGL y no tener retrocompatibilidad
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	//CREAR VENTANA
	mainWindow = glfwCreateWindow(width, height, "Proyecto Final", NULL, NULL);

	if (!mainWindow)
	{
		printf("Fallo en crearse la ventana con GLFW");
		glfwTerminate();
		return 1;
	}
	//Obtener tama�o de Buffer
	glfwGetFramebufferSize(mainWindow, &bufferWidth, &bufferHeight);

	//asignar el contexto
	glfwMakeContextCurrent(mainWindow);

	//MANEJAR TECLADO y MOUSE
	createCallbacks();


	//permitir nuevas extensiones
	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK)
	{
		printf("Fall� inicializaci�n de GLEW");
		glfwDestroyWindow(mainWindow);
		glfwTerminate();
		return 1;
	}

	glEnable(GL_DEPTH_TEST); //HABILITAR BUFFER DE PROFUNDIDAD
							 // Asignar valores de la ventana y coordenadas
							 
							 //Asignar Viewport
	glViewport(0, 0, bufferWidth, bufferHeight);
	//Callback para detectar que se est� usando la ventana
	glfwSetWindowUserPointer(mainWindow, this);
}

void Window::createCallbacks()
{
	glfwSetKeyCallback(mainWindow, ManejaTeclado);
	glfwSetCursorPosCallback(mainWindow, ManejaMouse);
}
GLfloat Window::getXChange()
{
	GLfloat theChange = xChange;
	xChange = 0.0f;
	return theChange;
}

GLfloat Window::getYChange()
{
	GLfloat theChange = yChange;
	yChange = 0.0f;
	return theChange;
}

void Window::ManejaTeclado(GLFWwindow* window, int key, int code, int action, int mode)
{
	Window* theWindow = static_cast<Window*>(glfwGetWindowUserPointer(window));

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	//Movmiento
	//Posici�n
	if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
	{
		theWindow->x += 1.0f;
		printf("x = %f", theWindow->x);
	}
	if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
	{
		theWindow->x -= 1.0f;
		printf("x = %f", theWindow->x);
	}
	if (key == GLFW_KEY_UP && action == GLFW_PRESS)
	{
		theWindow->z += 1.0f;
		printf("z = %f", theWindow-> z);
	}
	if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
	{
		theWindow->z -= 1.0f;
		printf("z = %f", theWindow->z);
	}
	if (key == GLFW_KEY_O && action == GLFW_PRESS)
	{
		theWindow->y += 0.01f;
		printf("y = %f", theWindow->y);
	}
	if (key == GLFW_KEY_L && action == GLFW_PRESS)
	{
		theWindow->y -= 0.01f;
		printf("y = %f", theWindow->y);
	}
	/*//Brazo derecho
	if (key == GLFW_KEY_1 && action == GLFW_PRESS)
	{
		theWindow->rotateBraDer -= 5.0f;
		printf("angulo = %f", theWindow->rotateBraDer);
	}
	if (key == GLFW_KEY_2 && action == GLFW_PRESS)
	{
		theWindow->rotateBraDer += 5.0f;
		printf("angulo = %f", theWindow->rotateBraDer);
	}
	//Brazo izquierdo
	if (key == GLFW_KEY_3)
	{
		theWindow->rotateBraIzq -= 5.0f;
	}
	if (key == GLFW_KEY_4)
	{
		theWindow->rotateBraIzq += 5.0f;
	}
	//Pierna derecha
	if (key == GLFW_KEY_5)
	{
		theWindow->rotatePieDer -= 5.0f;
	}
	if (key == GLFW_KEY_6)
	{
		theWindow->rotatePieDer += 5.0f;
	}
	//Pierna izquierda
	if (key == GLFW_KEY_7)
	{
		theWindow->rotatePieIzq -= 5.0f;
	}
	if (key == GLFW_KEY_8)
	{
		theWindow->rotatePieIzq += 5.0f;
	}
	*/

	//Animaciones
	if (key == GLFW_KEY_B && action == GLFW_PRESS)
	{	
		theWindow->animJuegos = !(theWindow->animJuegos);
	}
	if (key == GLFW_KEY_N && action == GLFW_PRESS)
	{
		theWindow->animBaile = !(theWindow->animBaile);
	}
	if (key == GLFW_KEY_M && action == GLFW_PRESS)
	{
		theWindow->animSnoopy = !(theWindow->animSnoopy);
	}

	//Luces Kiosko
	if (key == GLFW_KEY_I && action == GLFW_PRESS)
	{
		theWindow->lucesKiosko = !(theWindow->lucesKiosko);
	}
	//Animaci�n Luces Kiosko
	if (key == GLFW_KEY_O && action == GLFW_PRESS)
	{
		theWindow->animacionKiosko = !(theWindow->animacionKiosko);
		theWindow->animBaile = true;
	}

	//Linterna
	if (key == GLFW_KEY_P && action == GLFW_PRESS)
	{
		if (theWindow->luzLinterna == 1.0f) {
			theWindow->luzLinterna = 0.0f;
		}
		else {
			theWindow->luzLinterna = 1.0f;
		}
	}

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
		{
			theWindow->keys[key] = true;
			printf("se presiono la tecla %d'\n", key);
		}
		else if (action == GLFW_RELEASE)
		{
			theWindow->keys[key] = false;
			printf("se solto la tecla %d'\n", key);
		}
	}
}

void Window::ManejaMouse(GLFWwindow* window, double xPos, double yPos)
{
	Window* theWindow = static_cast<Window*>(glfwGetWindowUserPointer(window));

	if (theWindow->mouseFirstMoved)
	{
		theWindow->lastX = xPos;
		theWindow->lastY = yPos;
		theWindow->mouseFirstMoved = false;
	}

	theWindow->xChange = xPos - theWindow->lastX;
	theWindow->yChange = theWindow->lastY - yPos;

	theWindow->lastX = xPos;
	theWindow->lastY = yPos;
}

Window::~Window()
{
	glfwDestroyWindow(mainWindow);
	glfwTerminate();

}
